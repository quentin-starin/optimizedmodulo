﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;



namespace OptimizedModulo.Tests
{
    public partial class OptimizedModulo
    {
        [Theory]
        [InlineData((uint)0)]
        [InlineData((uint)1)]
        [InlineData((uint)2)]
        [InlineData((uint)3)]
        [InlineData((uint)4)]
        [InlineData((uint)5)]
        [InlineData((uint)6)]
        [InlineData((uint)7)]
        [InlineData((uint)8)]
        [InlineData((uint)9)]
        [InlineData((uint)10)]
        [InlineData((uint)65528)]
        [InlineData((uint)65529)]
        [InlineData((uint)65530)]
        [InlineData((uint)65531)]
        [InlineData((uint)65532)]
        [InlineData((uint)65533)]
        [InlineData((uint)65534)]
        [InlineData((uint)65535)]
        [InlineData((uint)65536)]
        [InlineData((uint)65537)]
        [InlineData((uint)65538)]
        [InlineData((uint)65539)]
        [InlineData((uint)65540)]
        [InlineData((uint)65541)]
        [InlineData((uint)65542)]
        [InlineData((uint)2147483642)]
        [InlineData((uint)2147483643)]
        [InlineData((uint)2147483644)]
        [InlineData((uint)2147483645)]
        [InlineData((uint)2147483646)]
        [InlineData((uint)2147483647)]
        [InlineData((uint)2147483648)]
        [InlineData((uint)2147483649)]
        [InlineData((uint)2147483650)]
        [InlineData((uint)2147483651)]
        [InlineData((uint)2147483652)]
        [InlineData((uint)4294967285)]
        [InlineData((uint)4294967286)]
        [InlineData((uint)4294967287)]
        [InlineData((uint)4294967288)]
        [InlineData((uint)4294967289)]
        [InlineData((uint)4294967290)]
        [InlineData((uint)4294967291)]
        [InlineData((uint)4294967292)]
        [InlineData((uint)4294967293)]
        [InlineData((uint)4294967294)]
        [InlineData((uint)4294967295)]
        public void UInt_Mersenne_0005(uint d)
        {
            Assert.Equal(d % 5, Modulo.Implementations.Int.Mersenne5(d));
            //Assert.Equal(d % 5, Mersenne5_3((uint)d));
        }

        private int Mersenne5_1(uint a)
        {
            a = (a >> 16) + (a & 0xFFFF); /* sum base 2**16 digits */
            a = (a >> 8) + (a & 0xFF);   /* sum base 2**8 digits */
            a = (a >> 4) + (a & 0xF);    /* sum base 2**4 digits */
            if (a > 14) a = a - 15;
            if (a > 14) a = a - 15;
            if (a > 14) a = a - 15;
            /* now, we have mod 15 */
            if (a > 4) a = a - 5;
            if (a > 4) a = a - 5;
            return (int)a;
        }

        private int Mersenne5_2(uint a)
        {
            a = (a >> 16) + (a & 0xFFFF); /* sum base 2**16 digits */
            a = (a >> 8) + (a & 0xFF);   /* sum base 2**8 digits */
            a = (a >> 4) + (a & 0xF);    /* sum base 2**4 digits */
            if (a > 14) a = a - 15;
            if (a > 14) a = a - 15;
            if (a > 14) a = a - 15;
            /* now, we have mod 15 */
            if (a > 9) a = a - 10;
            else if (a > 4) a = a - 5;
            return (int)a;
        }

        private int Mersenne5_3(uint a)
        {
            a = (a >> 16) + (a & 0xFFFF); /* sum base 2**16 digits */
            a = (a >> 8) + (a & 0xFF);   /* sum base 2**8 digits */
            a = (a >> 4) + (a & 0xF);    /* sum base 2**4 digits */
            if (a > 14) a = a - 15;
            if (a > 14) a = a - 15;
            if (a > 14) a = a - 15;
            /* now, we have mod 15 */
            if (a > 9) return (int)(a - 10);
            if (a > 4) return (int)(a - 5);
            return (int)a;
        }
    }
}
