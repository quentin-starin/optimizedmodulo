﻿using System;
using System.Collections.Generic;
using System.Text;
using BenchmarkDotNet.Attributes;


namespace OptimizedModulo.Benchmarks
{
    public class ModuloSelection
    {
        public ModuloSelection()
        {
            var mod = IfPow2Case(2);
            mod(8);
            mod = Case(2);
            mod(8);
            mod = Table(2);
            mod(8);
            mod = TableWithLargePow2(2);
            mod(8);
            mod = TableWithLargePow2AndMersenne(2);
            mod(8);
        }


        [Benchmark] public void Div1_Case() { Case(1); }
        [Benchmark] public void Div3_Case() { Case(3); }
        [Benchmark] public void Div4_Case() { Case(4); }
        [Benchmark] public void Div13_Case() { Case(13); }
        [Benchmark] public void DivMaxPow2_Case() { Case(0b1000000000000000000000000000000000000000000000000000000000000000); }
        [Benchmark] public void Div126_Case() { Case(126); }
        [Benchmark] public void Div128_Case() { Case(128); }
        [Benchmark] public void Div200_Case() { Case(200); }
        [Benchmark] public void Div1023_Case() { Case(1023); }



        [Benchmark] public void Div1_IfPow2Case() { IfPow2Case(1); }
        [Benchmark] public void Div3_IfPow2Case() { IfPow2Case(3); }
        [Benchmark] public void Div4_IfPow2Case() { IfPow2Case(4); }
        [Benchmark] public void Div13_IfPow2Case() { IfPow2Case(13); }
        [Benchmark] public void DivMaxPow2_IfPow2Case() { IfPow2Case(0b1000000000000000000000000000000000000000000000000000000000000000); }
        [Benchmark] public void Div126_IfPow2Case() { IfPow2Case(126); }
        [Benchmark] public void Div128_IfPow2Case() { IfPow2Case(128); }
        [Benchmark] public void Div200_IfPow2Case() { IfPow2Case(200); }
        [Benchmark] public void Div1023_IfPow2Case() { IfPow2Case(1023); }



        [Benchmark] public void Div1_Table() { Table(1); }
        [Benchmark] public void Div3_Table() { Table(3); }
        [Benchmark] public void Div4_Table() { Table(4); }
        [Benchmark] public void Div13_Table() { Table(13); }
        [Benchmark] public void DivMaxPow2_Table() { Table(0b1000000000000000000000000000000000000000000000000000000000000000); }
        [Benchmark] public void Div126_Table() { Table(126); }
        [Benchmark] public void Div128_Table() { Table(128); }
        [Benchmark] public void Div200_Table() { Table(200); }
        [Benchmark] public void Div1023_Table() { Table(1023); }



        [Benchmark] public void Div1_TableWithLargePow2() { TableWithLargePow2(1); }
        [Benchmark] public void Div3_TableWithLargePow2() { TableWithLargePow2(3); }
        [Benchmark] public void Div4_TableWithLargePow2() { TableWithLargePow2(4); }
        [Benchmark] public void Div13_TableWithLargePow2() { TableWithLargePow2(13); }
        [Benchmark] public void DivMaxPow2_TableWithLargePow2() { TableWithLargePow2(0b1000000000000000000000000000000000000000000000000000000000000000); }
        [Benchmark] public void Div126_TableWithLargePow2() { TableWithLargePow2(126); }
        [Benchmark] public void Div128_TableWithLargePow2() { TableWithLargePow2(128); }
        [Benchmark] public void Div200_TableWithLargePow2() { TableWithLargePow2(200); }
        [Benchmark] public void Div1023_TableWithLargePow2() { TableWithLargePow2(1023); }



        [Benchmark] public void Div1_TableWithLargePow2AndMersenne() { TableWithLargePow2AndMersenne(1); }
        [Benchmark] public void Div3_TableWithLargePow2AndMersenne() { TableWithLargePow2AndMersenne(3); }
        [Benchmark] public void Div4_TableWithLargePow2AndMersenne() { TableWithLargePow2AndMersenne(4); }
        [Benchmark] public void Div13_TableWithLargePow2AndMersenne() { TableWithLargePow2AndMersenne(13); }
        [Benchmark] public void DivMaxPow2_TableWithLargePow2AndMersenne() { TableWithLargePow2AndMersenne(0b1000000000000000000000000000000000000000000000000000000000000000); }
        [Benchmark] public void Div126_TableWithLargePow2AndMersenne() { TableWithLargePow2AndMersenne(126); }
        [Benchmark] public void Div128_TableWithLargePow2AndMersenne() { TableWithLargePow2AndMersenne(128); }
        [Benchmark] public void Div200_TableWithLargePow2AndMersenne() { TableWithLargePow2AndMersenne(200); }
        [Benchmark] public void Div1023_TableWithLargePow2AndMersenne() { TableWithLargePow2AndMersenne(1023); }


        static private Func<ulong, ulong> Case(ulong divisor)
        {
            switch (divisor)
            {
                case 1: return Modulo.Implementations.Long.One;
                case 3: return Modulo.Implementations.Long.Mersenne3;
                //case 4: Shift/And
                case 5: return Modulo.Implementations.Long.Mersenne5;
                case 6: return Modulo.Implementations.Long.Mersenne6;
                case 7: return Modulo.Implementations.Long.Mersenne7;
                //case 8: Shift/And
                case 10: return Modulo.Implementations.Long.Mersenne10;
                case 12: return Modulo.Implementations.Long.Mersenne12;
                case 14: return Modulo.Implementations.Long.Mersenne14;
                case 15: return Modulo.Implementations.Long.Mersenne15;
                //case 16: Shift/And
                case 20: return Modulo.Implementations.Long.Mersenne20;
                case 21: return Modulo.Implementations.Long.Mersenne21;
                case 24: return Modulo.Implementations.Long.Mersenne24;
                case 28: return Modulo.Implementations.Long.Mersenne28;
                case 30: return Modulo.Implementations.Long.Mersenne30;
                case 31: return Modulo.Implementations.Long.Mersenne31;
                //case 32: Shift/And
                case 35: return Modulo.Implementations.Long.Mersenne35;
                case 40: return Modulo.Implementations.Long.Mersenne40;
                case 42: return Modulo.Implementations.Long.Mersenne42;
                case 48: return Modulo.Implementations.Long.Mersenne48;
                case 56: return Modulo.Implementations.Long.Mersenne56;
                case 60: return Modulo.Implementations.Long.Mersenne60;
                case 62: return Modulo.Implementations.Long.Mersenne62;
                case 63: return Modulo.Implementations.Long.Mersenne63;
                //case 64: Shift/And
                case 70: return Modulo.Implementations.Long.Mersenne70;
                case 80: return Modulo.Implementations.Long.Mersenne80;
                case 84: return Modulo.Implementations.Long.Mersenne84;
                case 93: return Modulo.Implementations.Long.Mersenne93;
                case 96: return Modulo.Implementations.Long.Mersenne96;
                case 105: return Modulo.Implementations.Long.Mersenne105;
                case 112: return Modulo.Implementations.Long.Mersenne112;
                case 120: return Modulo.Implementations.Long.Mersenne120;
                case 124: return Modulo.Implementations.Long.Mersenne124;
                case 126: return Modulo.Implementations.Long.Mersenne126;
                case 127: return Modulo.Implementations.Long.Mersenne127;
                //case 128: Shift/And
                case 255: return Modulo.Implementations.Long.Mersenne255;
                //case 256: Shift/And
                case 511: return Modulo.Implementations.Long.Mersenne511;
                //case 512: Shift/And
                case 1023: return Modulo.Implementations.Long.Mersenne1023;
                //case 1024: Shift/And
                case 0b10:
                case 0b100:
                case 0b1000:
                case 0b10000:
                case 0b100000:
                case 0b1000000:
                case 0b10000000:
                case 0b100000000:
                case 0b1000000000:
                case 0b10000000000:
                case 0b100000000000:
                case 0b1000000000000:
                case 0b10000000000000:
                case 0b100000000000000:
                case 0b1000000000000000:
                case 0b10000000000000000:
                case 0b100000000000000000:
                case 0b1000000000000000000:
                case 0b10000000000000000000:
                case 0b100000000000000000000:
                case 0b1000000000000000000000:
                case 0b10000000000000000000000:
                case 0b100000000000000000000000:
                case 0b1000000000000000000000000:
                case 0b10000000000000000000000000:
                case 0b100000000000000000000000000:
                case 0b1000000000000000000000000000:
                case 0b10000000000000000000000000000:
                case 0b100000000000000000000000000000:
                case 0b1000000000000000000000000000000:
                case 0b10000000000000000000000000000000:
                case 0b100000000000000000000000000000000:
                case 0b1000000000000000000000000000000000:
                case 0b10000000000000000000000000000000000:
                case 0b100000000000000000000000000000000000:
                case 0b1000000000000000000000000000000000000:
                case 0b10000000000000000000000000000000000000:
                case 0b100000000000000000000000000000000000000:
                case 0b1000000000000000000000000000000000000000:
                case 0b10000000000000000000000000000000000000000:
                case 0b100000000000000000000000000000000000000000:
                case 0b1000000000000000000000000000000000000000000:
                case 0b10000000000000000000000000000000000000000000:
                case 0b100000000000000000000000000000000000000000000:
                case 0b1000000000000000000000000000000000000000000000:
                case 0b10000000000000000000000000000000000000000000000:
                case 0b100000000000000000000000000000000000000000000000:
                case 0b1000000000000000000000000000000000000000000000000:
                case 0b10000000000000000000000000000000000000000000000000:
                case 0b100000000000000000000000000000000000000000000000000:
                case 0b1000000000000000000000000000000000000000000000000000:
                case 0b10000000000000000000000000000000000000000000000000000:
                case 0b100000000000000000000000000000000000000000000000000000:
                case 0b1000000000000000000000000000000000000000000000000000000:
                case 0b10000000000000000000000000000000000000000000000000000000:
                case 0b100000000000000000000000000000000000000000000000000000000:
                case 0b1000000000000000000000000000000000000000000000000000000000:
                case 0b10000000000000000000000000000000000000000000000000000000000:
                case 0b100000000000000000000000000000000000000000000000000000000000:
                case 0b1000000000000000000000000000000000000000000000000000000000000:
                case 0b10000000000000000000000000000000000000000000000000000000000000:
                case 0b100000000000000000000000000000000000000000000000000000000000000:
                case 0b1000000000000000000000000000000000000000000000000000000000000000: // 2^64
                    return n => Modulo.Implementations.Long.ShiftAnd(n, divisor);
                default: return n => Modulo.Implementations.Long.ModuloOperator(n, divisor);
            }
        }

        static private Func<ulong, ulong> Table(ulong divisor)
        {
            return divisor < (ulong)_table.Length ? _table[divisor] : n => Modulo.Implementations.Long.ModuloOperator(n, divisor);
        }

        static private Func<ulong, ulong> TableWithLargePow2(ulong divisor)
        {
            return divisor < (ulong)_table.Length 
                    ? _table[divisor] 
                    : (divisor & (divisor - 1)) == 0
                        ? (Func<ulong, ulong>)(n => Modulo.Implementations.Long.ShiftAnd(n, divisor))
                        : n => Modulo.Implementations.Long.ModuloOperator(n, divisor);
        }

        static private Func<ulong, ulong> TableWithLargePow2AndMersenne(ulong divisor)
        {
            if (divisor < (ulong)_table.Length)
                return _table[divisor];

            if ((divisor & (divisor - 1)) == 0)
                return n => Modulo.Implementations.Long.ShiftAnd(n, divisor);

            if (divisor != 255 && divisor != 511 && divisor != 1023)
                return n => Modulo.Implementations.Long.ModuloOperator(n, divisor);

            if (divisor == 255)
                return Modulo.Implementations.Long.Mersenne255;

            if (divisor == 511)
                return Modulo.Implementations.Long.Mersenne511;

            if (divisor == 1023)
                return Modulo.Implementations.Long.Mersenne1023;

            return n => Modulo.Implementations.Long.ModuloOperator(n, divisor);
        }

        static private readonly Func<ulong, ulong>[] _table = {
            d => throw new DivideByZeroException(),
            Modulo.Implementations.Long.One,
            n => Modulo.Implementations.Long.ShiftAnd(n, 2),
            Modulo.Implementations.Long.Mersenne3,
            n => Modulo.Implementations.Long.ShiftAnd(n, 4),
            Modulo.Implementations.Long.Mersenne5,
            Modulo.Implementations.Long.Mersenne6,
            Modulo.Implementations.Long.Mersenne7,
            n => Modulo.Implementations.Long.ShiftAnd(n, 8),
            n => Modulo.Implementations.Long.ModuloOperator(n, 9),
            Modulo.Implementations.Long.Mersenne10,
            n => Modulo.Implementations.Long.ModuloOperator(n, 11),
            Modulo.Implementations.Long.Mersenne12,
            n => Modulo.Implementations.Long.ModuloOperator(n, 13),
            Modulo.Implementations.Long.Mersenne14,
            Modulo.Implementations.Long.Mersenne15,
            n => Modulo.Implementations.Long.ShiftAnd(n, 16),
            n => Modulo.Implementations.Long.ModuloOperator(n, 17),
            n => Modulo.Implementations.Long.ModuloOperator(n, 18),
            n => Modulo.Implementations.Long.ModuloOperator(n, 19),
            Modulo.Implementations.Long.Mersenne20,
            Modulo.Implementations.Long.Mersenne21,
            n => Modulo.Implementations.Long.ModuloOperator(n, 22),
            n => Modulo.Implementations.Long.ModuloOperator(n, 23),
            Modulo.Implementations.Long.Mersenne24,
            n => Modulo.Implementations.Long.ModuloOperator(n, 25),
            n => Modulo.Implementations.Long.ModuloOperator(n, 26),
            n => Modulo.Implementations.Long.ModuloOperator(n, 27),
            Modulo.Implementations.Long.Mersenne28,
            n => Modulo.Implementations.Long.ModuloOperator(n, 29),
            Modulo.Implementations.Long.Mersenne30,
            Modulo.Implementations.Long.Mersenne31,
            n => Modulo.Implementations.Long.ShiftAnd(n, 32),
            n => Modulo.Implementations.Long.ModuloOperator(n, 33),
            n => Modulo.Implementations.Long.ModuloOperator(n, 34),
            Modulo.Implementations.Long.Mersenne35,
            n => Modulo.Implementations.Long.ModuloOperator(n, 36),
            n => Modulo.Implementations.Long.ModuloOperator(n, 37),
            n => Modulo.Implementations.Long.ModuloOperator(n, 38),
            n => Modulo.Implementations.Long.ModuloOperator(n, 39),
            Modulo.Implementations.Long.Mersenne40,
            n => Modulo.Implementations.Long.ModuloOperator(n, 41),
            Modulo.Implementations.Long.Mersenne42,
            n => Modulo.Implementations.Long.ModuloOperator(n, 43),
            n => Modulo.Implementations.Long.ModuloOperator(n, 44),
            n => Modulo.Implementations.Long.ModuloOperator(n, 45),
            n => Modulo.Implementations.Long.ModuloOperator(n, 46),
            n => Modulo.Implementations.Long.ModuloOperator(n, 47),
            Modulo.Implementations.Long.Mersenne48,
            n => Modulo.Implementations.Long.ModuloOperator(n, 49),
            n => Modulo.Implementations.Long.ModuloOperator(n, 50),
            n => Modulo.Implementations.Long.ModuloOperator(n, 51),
            n => Modulo.Implementations.Long.ModuloOperator(n, 52),
            n => Modulo.Implementations.Long.ModuloOperator(n, 53),
            n => Modulo.Implementations.Long.ModuloOperator(n, 54),
            n => Modulo.Implementations.Long.ModuloOperator(n, 55),
            Modulo.Implementations.Long.Mersenne56,
            n => Modulo.Implementations.Long.ModuloOperator(n, 57),
            n => Modulo.Implementations.Long.ModuloOperator(n, 58),
            n => Modulo.Implementations.Long.ModuloOperator(n, 59),
            Modulo.Implementations.Long.Mersenne60,
            n => Modulo.Implementations.Long.ModuloOperator(n, 61),
            Modulo.Implementations.Long.Mersenne62,
            Modulo.Implementations.Long.Mersenne63,
            n => Modulo.Implementations.Long.ShiftAnd(n, 64),
            n => Modulo.Implementations.Long.ModuloOperator(n, 65),
            n => Modulo.Implementations.Long.ModuloOperator(n, 66),
            n => Modulo.Implementations.Long.ModuloOperator(n, 67),
            n => Modulo.Implementations.Long.ModuloOperator(n, 68),
            n => Modulo.Implementations.Long.ModuloOperator(n, 69),
            Modulo.Implementations.Long.Mersenne70,
            n => Modulo.Implementations.Long.ModuloOperator(n, 71),
            n => Modulo.Implementations.Long.ModuloOperator(n, 72),
            n => Modulo.Implementations.Long.ModuloOperator(n, 73),
            n => Modulo.Implementations.Long.ModuloOperator(n, 74),
            n => Modulo.Implementations.Long.ModuloOperator(n, 75),
            n => Modulo.Implementations.Long.ModuloOperator(n, 76),
            n => Modulo.Implementations.Long.ModuloOperator(n, 77),
            n => Modulo.Implementations.Long.ModuloOperator(n, 78),
            n => Modulo.Implementations.Long.ModuloOperator(n, 79),
            Modulo.Implementations.Long.Mersenne80,
            n => Modulo.Implementations.Long.ModuloOperator(n, 81),
            n => Modulo.Implementations.Long.ModuloOperator(n, 82),
            n => Modulo.Implementations.Long.ModuloOperator(n, 83),
            Modulo.Implementations.Long.Mersenne84,
            n => Modulo.Implementations.Long.ModuloOperator(n, 85),
            n => Modulo.Implementations.Long.ModuloOperator(n, 86),
            n => Modulo.Implementations.Long.ModuloOperator(n, 87),
            n => Modulo.Implementations.Long.ModuloOperator(n, 88),
            n => Modulo.Implementations.Long.ModuloOperator(n, 89),
            n => Modulo.Implementations.Long.ModuloOperator(n, 90),
            n => Modulo.Implementations.Long.ModuloOperator(n, 91),
            n => Modulo.Implementations.Long.ModuloOperator(n, 92),
            Modulo.Implementations.Long.Mersenne93,
            n => Modulo.Implementations.Long.ModuloOperator(n, 94),
            n => Modulo.Implementations.Long.ModuloOperator(n, 95),
            Modulo.Implementations.Long.Mersenne96,
            n => Modulo.Implementations.Long.ModuloOperator(n, 97),
            n => Modulo.Implementations.Long.ModuloOperator(n, 98),
            n => Modulo.Implementations.Long.ModuloOperator(n, 99),
            n => Modulo.Implementations.Long.ModuloOperator(n, 100),
            n => Modulo.Implementations.Long.ModuloOperator(n, 101),
            n => Modulo.Implementations.Long.ModuloOperator(n, 102),
            n => Modulo.Implementations.Long.ModuloOperator(n, 103),
            n => Modulo.Implementations.Long.ModuloOperator(n, 104),
            Modulo.Implementations.Long.Mersenne105,
            n => Modulo.Implementations.Long.ModuloOperator(n, 106),
            n => Modulo.Implementations.Long.ModuloOperator(n, 107),
            n => Modulo.Implementations.Long.ModuloOperator(n, 108),
            n => Modulo.Implementations.Long.ModuloOperator(n, 109),
            n => Modulo.Implementations.Long.ModuloOperator(n, 110),
            n => Modulo.Implementations.Long.ModuloOperator(n, 111),
            Modulo.Implementations.Long.Mersenne112,
            n => Modulo.Implementations.Long.ModuloOperator(n, 113),
            n => Modulo.Implementations.Long.ModuloOperator(n, 114),
            n => Modulo.Implementations.Long.ModuloOperator(n, 115),
            n => Modulo.Implementations.Long.ModuloOperator(n, 116),
            n => Modulo.Implementations.Long.ModuloOperator(n, 117),
            n => Modulo.Implementations.Long.ModuloOperator(n, 118),
            n => Modulo.Implementations.Long.ModuloOperator(n, 119),
            Modulo.Implementations.Long.Mersenne120,
            n => Modulo.Implementations.Long.ModuloOperator(n, 121),
            n => Modulo.Implementations.Long.ModuloOperator(n, 122),
            n => Modulo.Implementations.Long.ModuloOperator(n, 123),
            Modulo.Implementations.Long.Mersenne124,
            n => Modulo.Implementations.Long.ModuloOperator(n, 125),
            Modulo.Implementations.Long.Mersenne126,
            Modulo.Implementations.Long.Mersenne127,
            n => Modulo.Implementations.Long.ShiftAnd(n, 128),
        };

        static private Func<ulong, ulong> IfPow2Case(ulong divisor)
        {
            if ((divisor & (divisor - 1)) == 0)
                return n => Modulo.Implementations.Long.ShiftAnd(n, divisor);

            switch (divisor)
            {
                case 0: throw new DivideByZeroException();
                case 1: return Modulo.Implementations.Long.One;
                case 3: return Modulo.Implementations.Long.Mersenne3;
                //case 4: Shift/And
                case 5: return Modulo.Implementations.Long.Mersenne5;
                case 6: return Modulo.Implementations.Long.Mersenne6;
                case 7: return Modulo.Implementations.Long.Mersenne7;
                //case 8: Shift/And
                case 10: return Modulo.Implementations.Long.Mersenne10;
                case 12: return Modulo.Implementations.Long.Mersenne12;
                case 14: return Modulo.Implementations.Long.Mersenne14;
                case 15: return Modulo.Implementations.Long.Mersenne15;
                //case 16: Shift/And
                case 20: return Modulo.Implementations.Long.Mersenne20;
                case 21: return Modulo.Implementations.Long.Mersenne21;
                case 24: return Modulo.Implementations.Long.Mersenne24;
                case 28: return Modulo.Implementations.Long.Mersenne28;
                case 30: return Modulo.Implementations.Long.Mersenne30;
                case 31: return Modulo.Implementations.Long.Mersenne31;
                //case 32: Shift/And
                case 35: return Modulo.Implementations.Long.Mersenne35;
                case 40: return Modulo.Implementations.Long.Mersenne40;
                case 42: return Modulo.Implementations.Long.Mersenne42;
                case 48: return Modulo.Implementations.Long.Mersenne48;
                case 56: return Modulo.Implementations.Long.Mersenne56;
                case 60: return Modulo.Implementations.Long.Mersenne60;
                case 62: return Modulo.Implementations.Long.Mersenne62;
                case 63: return Modulo.Implementations.Long.Mersenne63;
                //case 64: Shift/And
                case 70: return Modulo.Implementations.Long.Mersenne70;
                case 80: return Modulo.Implementations.Long.Mersenne80;
                case 84: return Modulo.Implementations.Long.Mersenne84;
                case 93: return Modulo.Implementations.Long.Mersenne93;
                case 96: return Modulo.Implementations.Long.Mersenne96;
                case 105: return Modulo.Implementations.Long.Mersenne105;
                case 112: return Modulo.Implementations.Long.Mersenne112;
                case 120: return Modulo.Implementations.Long.Mersenne120;
                case 124: return Modulo.Implementations.Long.Mersenne124;
                case 126: return Modulo.Implementations.Long.Mersenne126;
                case 127: return Modulo.Implementations.Long.Mersenne127;
                //case 128: Shift/And
                case 255: return Modulo.Implementations.Long.Mersenne255;
                //case 256: Shift/And
                case 511: return Modulo.Implementations.Long.Mersenne511;
                //case 512: Shift/And
                case 1023: return Modulo.Implementations.Long.Mersenne1023;
                //case 1024: Shift/And
                default: return n => Modulo.Implementations.Long.ModuloOperator(n, divisor);
            }
        }
    }
}
