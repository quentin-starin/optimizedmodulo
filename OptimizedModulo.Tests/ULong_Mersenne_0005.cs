﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;



namespace OptimizedModulo.Tests
{
    public partial class OptimizedModulo
    {
        [Theory]
        [InlineData((uint)0)]
        [InlineData((uint)1)]
        [InlineData((uint)2)]
        [InlineData((uint)3)]
        [InlineData((uint)4)]
        [InlineData((uint)5)]
        [InlineData((uint)6)]
        [InlineData((uint)7)]
        [InlineData((uint)8)]
        [InlineData((uint)9)]
        [InlineData((uint)10)]
        [InlineData((uint)65528)]
        [InlineData((uint)65529)]
        [InlineData((uint)65530)]
        [InlineData((uint)65531)]
        [InlineData((uint)65532)]
        [InlineData((uint)65533)]
        [InlineData((uint)65534)]
        [InlineData((uint)65535)]
        [InlineData((uint)65536)]
        [InlineData((uint)65537)]
        [InlineData((uint)65538)]
        [InlineData((uint)65539)]
        [InlineData((uint)65540)]
        [InlineData((uint)65541)]
        [InlineData((uint)65542)]
        [InlineData((uint)2147483642)]
        [InlineData((uint)2147483643)]
        [InlineData((uint)2147483644)]
        [InlineData((uint)2147483645)]
        [InlineData((uint)2147483646)]
        [InlineData((uint)2147483647)]
        [InlineData((uint)2147483648)]
        [InlineData((uint)2147483649)]
        [InlineData((uint)2147483650)]
        [InlineData((uint)2147483651)]
        [InlineData((uint)2147483652)]
        [InlineData((uint)4294967288)]
        [InlineData((uint)4294967289)]
        [InlineData((uint)4294967290)]
        [InlineData((uint)4294967291)]
        [InlineData((uint)4294967292)]
        [InlineData((uint)4294967293)]
        [InlineData((uint)4294967294)]
        [InlineData((uint)4294967295)]
        [InlineData((ulong)4294967296)]
        [InlineData((ulong)4294967297)]
        [InlineData((ulong)4294967298)]
        [InlineData((ulong)4294967299)]
        [InlineData((ulong)4294967300)]
        [InlineData((ulong)4294967301)]
        [InlineData((ulong)4294967302)]
        [InlineData((ulong)9223372036854775800)]
        [InlineData((ulong)9223372036854775801)]
        [InlineData((ulong)9223372036854775802)]
        [InlineData((ulong)9223372036854775803)]
        [InlineData((ulong)9223372036854775804)]
        [InlineData((ulong)9223372036854775805)]
        [InlineData((ulong)9223372036854775806)]
        [InlineData((ulong)9223372036854775807)]
        [InlineData((ulong)9223372036854775808)]
        [InlineData((ulong)9223372036854775809)]
        [InlineData((ulong)9223372036854775810)]
        [InlineData((ulong)9223372036854775811)]
        [InlineData((ulong)9223372036854775812)]
        [InlineData((ulong)9223372036854775813)]
        [InlineData((ulong)9223372036854775814)]
        [InlineData((ulong)18446744073709551605)]
        [InlineData((ulong)18446744073709551606)]
        [InlineData((ulong)18446744073709551607)]
        [InlineData((ulong)18446744073709551608)]
        [InlineData((ulong)18446744073709551609)]
        [InlineData((ulong)18446744073709551610)]
        [InlineData((ulong)18446744073709551611)]
        [InlineData((ulong)18446744073709551612)]
        [InlineData((ulong)18446744073709551613)]
        [InlineData((ulong)18446744073709551614)]
        [InlineData((ulong)18446744073709551615)]
        public void ULong_Mersenne_0005(ulong d)
        {
            Assert.Equal(d % 5, Modulo.Implementations.Long.Mersenne5(d));
            //Assert.Equal(d % 5, Mersenne5_2_2(d));
        }

        private long Mersenne5_1(ulong a)
        {
            a = (a >> 32) + (a & 0xFFFFFFFF); // sum base 2**32 digits */
            a = (a >> 16) + (a & 0xFFFF); /* sum base 2**16 digits */
            a = (a >> 8) + (a & 0xFF);   /* sum base 2**8 digits */
            a = (a >> 4) + (a & 0xF);    /* sum base 2**4 digits */
            if (a > 14) a = a - 15;
            if (a > 14) a = a - 15;
            if (a > 14) a = a - 15;
            if (a > 14) a = a - 15;
            /* now, we have mod 15 */
            if (a > 4) a = a - 5;
            if (a > 4) a = a - 5;
            return (long)a;
        }

        private long Mersenne5_2(ulong a)
        {
            a = (a >> 32) + (a & 0xFFFFFFFF); // sum base 2**32 digits */
            a = (a >> 16) + (a & 0xFFFF); /* sum base 2**16 digits */
            a = (a >> 8) + (a & 0xFF);   /* sum base 2**8 digits */
            a = (a >> 4) + (a & 0xF);    /* sum base 2**4 digits */
            if (a > 14) a = a - 15;
            if (a > 14) a = a - 15;
            if (a > 14) a = a - 15;
            if (a > 14) a = a - 15;
            /* now, we have mod 15 */
            if (a > 9) a = a - 10;
            else if (a > 4) a = a - 5;
            return (long)a;
        }

        private long Mersenne5_2_2(ulong a)
        {
            a = (a >> 32) + (a & 0xFFFFFFFF); // sum base 2**32 digits */
            a = (a >> 16) + (a & 0xFFFF); /* sum base 2**16 digits */
            a = (a >> 8) + (a & 0xFF);   /* sum base 2**8 digits */
            a = (a >> 4) + (a & 0xF);    /* sum base 2**4 digits */
            if (a > 59) a = a - 60;
            else if (a > 44) a = a - 45;
            else if (a > 29) a = a - 30;
            else if (a > 14) a = a - 15;
            /* now, we have mod 15 */
            if (a > 9) a = a - 10;
            else if (a > 4) a = a - 5;
            return (long)a;
        }

        private long Mersenne5_3(ulong a)
        {
            a = (a >> 32) + (a & 0xFFFFFFFF); // sum base 2**32 digits */
            a = (a >> 16) + (a & 0xFFFF); /* sum base 2**16 digits */
            a = (a >> 8) + (a & 0xFF);   /* sum base 2**8 digits */
            a = (a >> 4) + (a & 0xF);    /* sum base 2**4 digits */
            if (a > 14) a = a - 15;
            if (a > 14) a = a - 15;
            if (a > 14) a = a - 15;
            if (a > 14) a = a - 15;
            /* now, we have mod 15 */
            if (a > 9) return (long)(a - 10);
            if (a > 4) return (long)(a - 5);
            return (long)a;
        }
    }
}
