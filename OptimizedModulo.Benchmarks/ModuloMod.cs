﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Attributes.Jobs;
using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Environments;
using BenchmarkDotNet.Jobs;


namespace OptimizedModulo.Benchmarks
{
    [Config(typeof(ModuloMod.Config))]
    public class ModuloMod
    {
        public class Config : ManualConfig
        {
            public Config()
            {
                Add(Job.Default
                       .With(Runtime.Clr)
                       .With(Platform.X64)
                       .WithId("netfx x64"));

                Add(Job.Default
                       .With(Runtime.Clr)
                       .With(Platform.X86)
                       .WithId("netfx x86"));
            }
        }

        public ulong rl;
        public uint ri;
        private ulong _lDiv5;
        private uint _iDiv5;
        private ulong _lDiv16;
        private uint _iDiv16;

        public ModuloMod()
        {
            var counter = new List<string> { "", "", "", ""};
            _lDiv5 = (ulong)counter.Count() + 1;
            _iDiv5 = (uint)counter.Count() + 1;
            _lDiv16 = (ulong)counter.Count() + 12;
            _iDiv16 = (uint)counter.Count() + 12;
        }


        [Benchmark(Baseline = true)]
        public ulong Long_ModOp_HC() 
        {
            for (ulong i = 0; i < 512; i++)
                rl += i % 5;

            //for (ulong i = ulong.MaxValue; i > ulong.MaxValue - 512; i--)
            //    rl += i % 5;

            return rl;
        }

        [Benchmark]
        public ulong Long_ModOp() 
        {
            for (ulong i = 0; i < 512; i++)
                rl += i % _iDiv5;

            //for (ulong i = ulong.MaxValue; i > ulong.MaxValue - 512; i--)
            //    rl += i % _iDiv5;

            return rl;
        }

        [Benchmark] public ulong Long_Mod() => LongHarness(n => Modulo.Implementations.Long.ModuloOperator(n, _lDiv5));
        [Benchmark] public ulong Long_Mersenne() => LongHarness(Modulo.Implementations.Long.Mersenne5);
        [Benchmark] public ulong Long_Pow2() => LongHarness(n => Modulo.Implementations.Long.ShiftAnd(n, _lDiv16));

        private ulong LongHarness(Func<ulong, ulong> mod)
        {
            for (ulong i = 0; i < 512; i++)
                rl += mod(i);

            //for (ulong i = ulong.MaxValue; i > ulong.MaxValue - 512; i--)
            //    rl += mod(i);

            return rl;
        }


        [Benchmark]
        public uint Int_ModOp_HC() 
        {
            for (uint i = 0; i < 512; i++)
                ri += i % 5;

            //for (uint i = uint.MaxValue; i > uint.MaxValue - 512; i--)
            //    rl += i % 5;

            return ri;
        }

        [Benchmark]
        public uint Int_ModOp() 
        {
            for (uint i = 0; i < 512; i++)
                ri += i % _iDiv5;

            //for (uint i = uint.MaxValue; i > uint.MaxValue - 512; i--)
            //    ri += i % _iDiv5;

            return ri;
        }

        [Benchmark] public uint Int_Mod() => IntHarness(n => Modulo.Implementations.Int.ModuloOperator(n, _iDiv5));
        [Benchmark] public uint Int_Mersenne() => IntHarness(Modulo.Implementations.Int.Mersenne5);
        [Benchmark] public uint Int_Pow2() => IntHarness(n => Modulo.Implementations.Int.ShiftAnd(n, _iDiv16));

        private uint IntHarness(Func<uint, uint> mod)
        {
            for (uint i = 0; i < 512; i++)
                ri += mod(i);

            //for (uint i = uint.MaxValue; i > uint.MaxValue - 512; i--)
            //    ri += mod(i);

            return ri;
        }
    }
}
