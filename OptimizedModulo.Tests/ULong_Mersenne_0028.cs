﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;



namespace OptimizedModulo.Tests
{
    public partial class OptimizedModulo
    {
        [Theory]
        [InlineData((uint)0)]
        [InlineData((uint)1)]
        [InlineData((uint)2)]
        [InlineData((uint)3)]
        [InlineData((uint)4)]
        [InlineData((uint)5)]
        [InlineData((uint)6)]
        [InlineData((uint)7)]
        [InlineData((uint)8)]
        [InlineData((uint)9)]
        [InlineData((uint)10)]
        [InlineData((uint)11)]
        [InlineData((uint)12)]
        [InlineData((uint)13)]
        [InlineData((uint)14)]
        [InlineData((uint)15)]
        [InlineData((uint)16)]
        [InlineData((uint)17)]
        [InlineData((uint)18)]
        [InlineData((uint)19)]
        [InlineData((uint)20)]
        [InlineData((uint)21)]
        [InlineData((uint)22)]
        [InlineData((uint)23)]
        [InlineData((uint)24)]
        [InlineData((uint)25)]
        [InlineData((uint)26)]
        [InlineData((uint)27)]
        [InlineData((uint)28)]
        [InlineData((uint)29)]
        [InlineData((uint)30)]
        [InlineData((uint)31)]
        [InlineData((uint)32)]
        [InlineData((uint)33)]
        [InlineData((uint)65516)]
        [InlineData((uint)65517)]
        [InlineData((uint)65518)]
        [InlineData((uint)65519)]
        [InlineData((uint)65520)]
        [InlineData((uint)65521)]
        [InlineData((uint)65522)]
        [InlineData((uint)65523)]
        [InlineData((uint)65524)]
        [InlineData((uint)65525)]
        [InlineData((uint)65526)]
        [InlineData((uint)65527)]
        [InlineData((uint)65528)]
        [InlineData((uint)65529)]
        [InlineData((uint)65530)]
        [InlineData((uint)65531)]
        [InlineData((uint)65532)]
        [InlineData((uint)65533)]
        [InlineData((uint)65534)]
        [InlineData((uint)65535)]
        [InlineData((uint)65536)]
        [InlineData((uint)65537)]
        [InlineData((uint)65538)]
        [InlineData((uint)65539)]
        [InlineData((uint)65540)]
        [InlineData((uint)65541)]
        [InlineData((uint)65542)]
        [InlineData((uint)65543)]
        [InlineData((uint)65544)]
        [InlineData((uint)65545)]
        [InlineData((uint)65546)]
        [InlineData((uint)65547)]
        [InlineData((uint)65548)]
        [InlineData((uint)65549)]
        [InlineData((uint)65550)]
        [InlineData((uint)65551)]
        [InlineData((uint)65552)]
        [InlineData((uint)65553)]
        [InlineData((uint)65554)]
        [InlineData((uint)2147483619)]
        [InlineData((uint)2147483620)]
        [InlineData((uint)2147483621)]
        [InlineData((uint)2147483622)]
        [InlineData((uint)2147483623)]
        [InlineData((uint)2147483624)]
        [InlineData((uint)2147483625)]
        [InlineData((uint)2147483626)]
        [InlineData((uint)2147483627)]
        [InlineData((uint)2147483628)]
        [InlineData((uint)2147483629)]
        [InlineData((uint)2147483630)]
        [InlineData((uint)2147483631)]
        [InlineData((uint)2147483632)]
        [InlineData((uint)2147483633)]
        [InlineData((uint)2147483634)]
        [InlineData((uint)2147483635)]
        [InlineData((uint)2147483636)]
        [InlineData((uint)2147483637)]
        [InlineData((uint)2147483638)]
        [InlineData((uint)2147483639)]
        [InlineData((uint)2147483640)]
        [InlineData((uint)2147483641)]
        [InlineData((uint)2147483642)]
        [InlineData((uint)2147483643)]
        [InlineData((uint)2147483644)]
        [InlineData((uint)2147483645)]
        [InlineData((uint)2147483646)]
        [InlineData((uint)2147483647)]
        [InlineData((uint)2147483648)]
        [InlineData((uint)2147483649)]
        [InlineData((uint)2147483650)]
        [InlineData((uint)2147483651)]
        [InlineData((uint)2147483652)]
        [InlineData((uint)2147483653)]
        [InlineData((uint)2147483654)]
        [InlineData((uint)2147483655)]
        [InlineData((uint)2147483656)]
        [InlineData((uint)2147483657)]
        [InlineData((uint)2147483658)]
        [InlineData((uint)2147483659)]
        [InlineData((uint)2147483660)]
        [InlineData((uint)2147483661)]
        [InlineData((uint)2147483662)]
        [InlineData((uint)2147483663)]
        [InlineData((uint)2147483664)]
        [InlineData((uint)2147483665)]
        [InlineData((uint)2147483666)]
        [InlineData((uint)2147483667)]
        [InlineData((uint)2147483668)]
        [InlineData((uint)2147483669)]
        [InlineData((uint)2147483670)]
        [InlineData((uint)2147483671)]
        [InlineData((uint)2147483672)]
        [InlineData((uint)2147483673)]
        [InlineData((uint)2147483674)]
        [InlineData((uint)2147483675)]
        [InlineData((uint)4294967276)]
        [InlineData((uint)4294967277)]
        [InlineData((uint)4294967278)]
        [InlineData((uint)4294967279)]
        [InlineData((uint)4294967280)]
        [InlineData((uint)4294967281)]
        [InlineData((uint)4294967282)]
        [InlineData((uint)4294967283)]
        [InlineData((uint)4294967284)]
        [InlineData((uint)4294967285)]
        [InlineData((uint)4294967286)]
        [InlineData((uint)4294967287)]
        [InlineData((uint)4294967288)]
        [InlineData((uint)4294967289)]
        [InlineData((uint)4294967290)]
        [InlineData((uint)4294967291)]
        [InlineData((uint)4294967292)]
        [InlineData((uint)4294967293)]
        [InlineData((uint)4294967294)]
        [InlineData((uint)4294967295)]
        [InlineData((ulong)4294967296)]
        [InlineData((ulong)4294967297)]
        [InlineData((ulong)4294967298)]
        [InlineData((ulong)4294967299)]
        [InlineData((ulong)4294967300)]
        [InlineData((ulong)4294967301)]
        [InlineData((ulong)4294967302)]
        [InlineData((ulong)4294967303)]
        [InlineData((ulong)4294967304)]
        [InlineData((ulong)4294967305)]
        [InlineData((ulong)4294967306)]
        [InlineData((ulong)4294967307)]
        [InlineData((ulong)4294967308)]
        [InlineData((ulong)4294967309)]
        [InlineData((ulong)4294967310)]
        [InlineData((ulong)4294967311)]
        [InlineData((ulong)4294967312)]
        [InlineData((ulong)4294967313)]
        [InlineData((ulong)4294967314)]
        [InlineData((ulong)9223372036854775788)]
        [InlineData((ulong)9223372036854775789)]
        [InlineData((ulong)9223372036854775790)]
        [InlineData((ulong)9223372036854775791)]
        [InlineData((ulong)9223372036854775792)]
        [InlineData((ulong)9223372036854775793)]
        [InlineData((ulong)9223372036854775794)]
        [InlineData((ulong)9223372036854775795)]
        [InlineData((ulong)9223372036854775796)]
        [InlineData((ulong)9223372036854775797)]
        [InlineData((ulong)9223372036854775798)]
        [InlineData((ulong)9223372036854775799)]
        [InlineData((ulong)9223372036854775800)]
        [InlineData((ulong)9223372036854775801)]
        [InlineData((ulong)9223372036854775802)]
        [InlineData((ulong)9223372036854775803)]
        [InlineData((ulong)9223372036854775804)]
        [InlineData((ulong)9223372036854775805)]
        [InlineData((ulong)9223372036854775806)]
        [InlineData((ulong)9223372036854775807)]
        [InlineData((ulong)9223372036854775808)]
        [InlineData((ulong)9223372036854775809)]
        [InlineData((ulong)9223372036854775810)]
        [InlineData((ulong)9223372036854775811)]
        [InlineData((ulong)9223372036854775812)]
        [InlineData((ulong)9223372036854775813)]
        [InlineData((ulong)9223372036854775814)]
        [InlineData((ulong)9223372036854775815)]
        [InlineData((ulong)9223372036854775816)]
        [InlineData((ulong)9223372036854775817)]
        [InlineData((ulong)9223372036854775818)]
        [InlineData((ulong)9223372036854775819)]
        [InlineData((ulong)9223372036854775820)]
        [InlineData((ulong)9223372036854775821)]
        [InlineData((ulong)9223372036854775822)]
        [InlineData((ulong)9223372036854775823)]
        [InlineData((ulong)9223372036854775824)]
        [InlineData((ulong)9223372036854775825)]
        [InlineData((ulong)9223372036854775826)]
        [InlineData((ulong)18446744073709551582)]
        [InlineData((ulong)18446744073709551583)]
        [InlineData((ulong)18446744073709551584)]
        [InlineData((ulong)18446744073709551585)]
        [InlineData((ulong)18446744073709551586)]
        [InlineData((ulong)18446744073709551587)]
        [InlineData((ulong)18446744073709551588)]
        [InlineData((ulong)18446744073709551589)]
        [InlineData((ulong)18446744073709551590)]
        [InlineData((ulong)18446744073709551591)]
        [InlineData((ulong)18446744073709551592)]
        [InlineData((ulong)18446744073709551593)]
        [InlineData((ulong)18446744073709551594)]
        [InlineData((ulong)18446744073709551595)]
        [InlineData((ulong)18446744073709551596)]
        [InlineData((ulong)18446744073709551597)]
        [InlineData((ulong)18446744073709551598)]
        [InlineData((ulong)18446744073709551599)]
        [InlineData((ulong)18446744073709551600)]
        [InlineData((ulong)18446744073709551601)]
        [InlineData((ulong)18446744073709551602)]
        [InlineData((ulong)18446744073709551603)]
        [InlineData((ulong)18446744073709551604)]
        [InlineData((ulong)18446744073709551605)]
        [InlineData((ulong)18446744073709551606)]
        [InlineData((ulong)18446744073709551607)]
        [InlineData((ulong)18446744073709551608)]
        [InlineData((ulong)18446744073709551609)]
        [InlineData((ulong)18446744073709551610)]
        [InlineData((ulong)18446744073709551611)]
        [InlineData((ulong)18446744073709551612)]
        [InlineData((ulong)18446744073709551613)]
        [InlineData((ulong)18446744073709551614)]
        [InlineData((ulong)18446744073709551615)]
        public void ULong_Mersenne_0028(ulong d)
        {
            Assert.Equal(d % 28, Modulo.Implementations.Long.Mersenne28(d));
        }
    }
}
