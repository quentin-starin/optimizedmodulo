﻿using System;
using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Environments;
using BenchmarkDotNet.Jobs;
using BenchmarkDotNet.Running;


namespace OptimizedModulo.Benchmarks
{
    class Program
    {
        static void Main(string[] args)
        {
            BenchmarkRunner.Run<ModuloBest>();
            //BenchmarkRunner.Run<ModuloMod>();
            //BenchmarkRunner.Run<ModuloSelection>();
        }
    }
}