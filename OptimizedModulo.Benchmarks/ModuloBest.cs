﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Environments;
using BenchmarkDotNet.Jobs;


namespace OptimizedModulo.Benchmarks
{
    [Config(typeof(ModuloBest.Config))]
    public class ModuloBest
    {
        public class Config : ManualConfig
        {
            public Config()
            {
                Add(Job.Default
                       .With(Runtime.Clr)
                       .With(Platform.X64)
                       .WithId("fx64"));

                Add(Job.Default
                       .With(Runtime.Clr)
                       .With(Platform.X86)
                       .WithId("fx86"));
            }
        }

        private ulong _lDiv3;
        private ulong _lDiv5;
        private ulong _lDiv6;
        private ulong _lDiv70;
        private ulong _lDiv128;
        private uint _iDiv3;
        private uint _iDiv5;
        private uint _iDiv6;
        private uint _iDiv70;
        private uint _iDiv128;

        public ModuloBest()
        {
            var counter = new List<string> { "", "" };
            _lDiv3 = (ulong)counter.Count() + 1;
            _lDiv5 = (ulong)counter.Count() + 3;
            _lDiv6 = (ulong)counter.Count() + 4;
            _lDiv70 = (ulong)counter.Count() + 68;
            _lDiv128 = (ulong)counter.Count() + 126;
            _iDiv3 = (uint)counter.Count() + 1;
            _iDiv5 = (uint)counter.Count() + 3;
            _iDiv6 = (uint)counter.Count() + 4;
            _iDiv70 = (uint)counter.Count() + 68;
            _iDiv128 = (uint)counter.Count() + 126;
        }


        [Benchmark(Baseline = true)]
        public ulong Modulo_HC_3_Long() => HarnessModuloHC_Long(_lDiv3);

        [Benchmark]
        public ulong Mersenne_HC_3_Long() => HarnessMersenneHC_3_Long();

        [Benchmark]
        public ulong Modulo_3_Long() => Harness_Long(n => Modulo.Implementations.Long.ModuloOperator(n, _lDiv3));

        [Benchmark]
        public ulong Mersenne_3_Long() => Harness_Long(Modulo.Implementations.Long.Mersenne3);


        [Benchmark]
        public ulong Modulo_HC_5_Long() => HarnessModuloHC_Long(_lDiv5);

        [Benchmark]
        public ulong Mersenne_HC_5_Long() => HarnessMersenneHC_5_Long();

        [Benchmark]
        public ulong Modulo_5_Long() => Harness_Long(n => Modulo.Implementations.Long.ModuloOperator(n, _lDiv5));

        [Benchmark]
        public ulong Mersenne_5_Long() => Harness_Long(Modulo.Implementations.Long.Mersenne5);


        [Benchmark]
        public ulong Modulo_HC_6_Long() => HarnessModuloHC_Long(_lDiv6);

        [Benchmark]
        public ulong Mersenne_HC_6_Long() => HarnessMersenneHC_6_Long();

        [Benchmark]
        public ulong Modulo_6_Long() => Harness_Long(n => Modulo.Implementations.Long.ModuloOperator(n, _lDiv6));

        [Benchmark]
        public ulong Mersenne_6_Long() => Harness_Long(Modulo.Implementations.Long.Mersenne6);


        [Benchmark]
        public ulong Modulo_HC_70_Long() => HarnessModuloHC_Long(_lDiv70);

        [Benchmark]
        public ulong Mersenne_HC_70_Long() => HarnessMersenneHC_70_Long();

        [Benchmark]
        public ulong Modulo_70_Long() => Harness_Long(n => Modulo.Implementations.Long.ModuloOperator(n, _lDiv70));

        [Benchmark]
        public ulong Mersenne_70_Long() => Harness_Long(Modulo.Implementations.Long.Mersenne70);


        [Benchmark]
        public ulong Modulo_HC_128_Long() => HarnessModuloHC_Long(_lDiv128);

        [Benchmark]
        public ulong Shift_HC_128_Long() => HarnessShiftHC_Long(128);

        [Benchmark]
        public ulong Shift_HC2_128_Long() => HarnessShiftHC2_128_Long();

        [Benchmark]
        public ulong Modulo_128_Long() => Harness_Long(n => Modulo.Implementations.Long.ModuloOperator(n, _lDiv128));

        [Benchmark]
        public ulong Shift_128_Long() => Harness_Long(n => Modulo.Implementations.Long.ShiftAnd(n, 128));



        private ulong Harness_Long(Func<ulong, ulong> mod)
        {
            ulong rl = 0;
            for (ulong i = ulong.MaxValue; i > ulong.MaxValue - 512; i--)
                rl += mod(i);

            return rl;
        }

        private ulong HarnessModuloHC_Long(ulong divisor)
        {
            ulong rl = 0;
            for (ulong i = ulong.MaxValue; i > ulong.MaxValue - 512; i--)
                rl += i % divisor;

            return rl;
        }

        private ulong HarnessShiftHC_Long(ulong divisor)
        {
            ulong rl = 0;
            for (ulong i = ulong.MaxValue; i > ulong.MaxValue - 512; i--)
                rl += i & (divisor - 1);

            return rl;
        }

        private ulong HarnessShiftHC2_128_Long()
        {
            ulong rl = 0;
            for (ulong i = ulong.MaxValue; i > ulong.MaxValue - 512; i--)
                rl += i & 127;

            return rl;
        }

        private ulong HarnessMersenneHC_3_Long()
        {
            ulong rl = 0;
            ulong mod3 = 0;
            for (ulong i = ulong.MaxValue; i > ulong.MaxValue - 512; i--) {
                mod3 = (i >> 32) + (i & 0xFFFFFFFF);
                mod3 = (mod3 >> 16) + (mod3 & 0xFFFF);
                mod3 = (mod3 >> 8) + (mod3 & 0xFF);
                mod3 = (mod3 >> 4) + (mod3 & 0xF);
                mod3 = (mod3 >> 2) + (mod3 & 0x3);
                mod3 = (mod3 >> 2) + (mod3 & 0x3);
                mod3 = (mod3 >> 2) + (mod3 & 0x3);
                if (mod3 > 2) mod3 = mod3 - 3;
                rl += mod3;
            }

            return rl;
        }

        private ulong HarnessMersenneHC_5_Long()
        {
            ulong rl = 0;
            ulong mod5 = 0;
            for (ulong i = ulong.MaxValue; i > ulong.MaxValue - 512; i--) {
                mod5 = (i >> 32) + (i & 0xFFFFFFFF);
                mod5 = (mod5 >> 16) + (mod5 & 0xFFFF);
                mod5 = (mod5 >> 8) + (mod5 & 0xFF);
                mod5 = (mod5 >> 4) + (mod5 & 0xF);
                mod5 = (mod5 >> 4) + (mod5 & 0xF);
                if (mod5 > 14) { mod5 = mod5 - 15; } // mod 15
                if (mod5 > 10) { mod5 = mod5 - 10; }
                if (mod5 > 4) { mod5 = mod5 - 5; }
                rl += mod5;
            }

            return rl;
        }

        static private readonly ulong[] _lmersenne6Table = { 0, 3, 4, 1, 2, 5 };
        private ulong HarnessMersenneHC_6_Long()
        {
            ulong rl = 0;
            ulong mod3 = 0;
            for (ulong i = ulong.MaxValue; i > ulong.MaxValue - 512; i--) {
                mod3 = (i >> 32) + (i & 0xFFFFFFFF);
                mod3 = (mod3 >> 16) + (mod3 & 0xFFFF);
                mod3 = (mod3 >> 8) + (mod3 & 0xFF);
                mod3 = (mod3 >> 4) + (mod3 & 0xF);
                mod3 = (mod3 >> 2) + (mod3 & 0x3);
                mod3 = (mod3 >> 2) + (mod3 & 0x3);
                mod3 = (mod3 >> 2) + (mod3 & 0x3);
                if (mod3 > 2) mod3 = mod3 - 3;
                rl += _lmersenne6Table[(mod3 << 1) | (i & 0b1)];
            }

            return rl;
        }

        static private readonly ulong[] _lmersenne70Table = { 0, 35, 50, 15, 30, 65, 10, 45, 60, 25, 40, 5, 20, 55, 0, 0, 56, 21, 36, 1, 16, 51, 66, 31, 46, 11, 26, 61, 6, 41, 0, 0, 42, 7, 22, 57, 2, 37, 52, 17, 32, 67, 12, 47, 62, 27, 0, 0, 28, 63, 8, 43, 58, 23, 38, 3, 18, 53, 68, 33, 48, 13, 0, 0, 14, 49, 64, 29, 44, 9, 24, 59, 4, 39, 54, 19, 34, 69 };
        private ulong HarnessMersenneHC_70_Long()
        {
            ulong rl = 0;
            ulong mod5 = 0, mod7 = 0;
            for (ulong i = ulong.MaxValue; i > ulong.MaxValue - 512; i--) {
                mod7 = (i >> 48) + (i & 0xFFFFFFFFFFFF);
                mod7 = (mod7 >> 24) + (mod7 & 0xFFFFFF);
                mod7 = (mod7 >> 12) + (mod7 & 0xFFF);
                mod7 = (mod7 >> 6) + (mod7 & 0x3F);
                mod7 = (mod7 >> 3) + (mod7 & 0x7);
                mod7 = (mod7 >> 3) + (mod7 & 0x7);
                if (mod7 > 6) mod7 = mod7 - 7;
                mod5 = (i >> 32) + (i & 0xFFFFFFFF);
                mod5 = (mod5 >> 16) + (mod5 & 0xFFFF);
                mod5 = (mod5 >> 8) + (mod5 & 0xFF);
                mod5 = (mod5 >> 4) + (mod5 & 0xF);
                mod5 = (mod5 >> 4) + (mod5 & 0xF);
                if (mod5 > 14) mod5 = mod5 - 15;
                if (mod5 > 9) mod5 = mod5 - 10;
                if (mod5 > 4) mod5 = mod5 - 5;
                rl += _lmersenne70Table[(mod5 << 4) | (mod7 << 1) | (i & 0b1)];
            }

            return rl;
        }




        [Benchmark]
        public uint Modulo_HC_3_Int() => HarnessModuloHC_Int(_iDiv3);

        [Benchmark]
        public uint Mersenne_HC_3_Int() => HarnessMersenneHC_3_Int();

        [Benchmark]
        public uint Modulo_3_Int() => Harness_Int(n => Modulo.Implementations.Int.ModuloOperator(n, _iDiv3));

        [Benchmark]
        public uint Mersenne_3_Int() => Harness_Int(Modulo.Implementations.Int.Mersenne3);


        [Benchmark]
        public uint Modulo_HC_5_Int() => HarnessModuloHC_Int(_iDiv5);

        [Benchmark]
        public uint Mersenne_HC_5_Int() => HarnessMersenneHC_5_Int();

        [Benchmark]
        public uint Modulo_5_Int() => Harness_Int(n => Modulo.Implementations.Int.ModuloOperator(n, _iDiv5));

        [Benchmark]
        public uint Mersenne_5_Int() => Harness_Int(Modulo.Implementations.Int.Mersenne5);


        [Benchmark]
        public uint Modulo_HC_6_Int() => HarnessModuloHC_Int(_iDiv6);

        [Benchmark]
        public uint Mersenne_HC_6_Int() => HarnessMersenneHC_6_Int();

        [Benchmark]
        public uint Modulo_6_Int() => Harness_Int(n => Modulo.Implementations.Int.ModuloOperator(n, _iDiv6));

        [Benchmark]
        public uint Mersenne_6_Int() => Harness_Int(Modulo.Implementations.Int.Mersenne6);


        [Benchmark]
        public uint Modulo_HC_70_Int() => HarnessModuloHC_Int(_iDiv70);

        [Benchmark]
        public uint Mersenne_HC_70_Int() => HarnessMersenneHC_70_Int();

        [Benchmark]
        public uint Modulo_70_Int() => Harness_Int(n => Modulo.Implementations.Int.ModuloOperator(n, _iDiv70));

        [Benchmark]
        public uint Mersenne_70_Int() => Harness_Int(Modulo.Implementations.Int.Mersenne70);


        [Benchmark]
        public uint Modulo_HC_128_Int() => HarnessModuloHC_Int(_iDiv128);

        [Benchmark]
        public uint Shift_HC_128_Int() => HarnessShiftHC_Int(128);

        [Benchmark]
        public uint Shift_HC2_128_Int() => HarnessShiftHC2_128_Int();

        [Benchmark]
        public uint Modulo_128_Int() => Harness_Int(n => Modulo.Implementations.Int.ModuloOperator(n, _iDiv128));

        [Benchmark]
        public uint Shift_128_Int() => Harness_Int(n => Modulo.Implementations.Int.ShiftAnd(n, 128));



        private uint Harness_Int(Func<uint, uint> mod)
        {
            uint rl = 0;
            for (uint i = uint.MaxValue; i > uint.MaxValue - 512; i--)
                rl += mod(i);

            return rl;
        }

        private uint HarnessModuloHC_Int(uint divisor)
        {
            uint rl = 0;
            for (uint i = uint.MaxValue; i > uint.MaxValue - 512; i--)
                rl += i % divisor;

            return rl;
        }

        private uint HarnessShiftHC_Int(uint divisor)
        {
            uint rl = 0;
            for (uint i = uint.MaxValue; i > uint.MaxValue - 512; i--)
                rl += i & (divisor - 1);

            return rl;
        }

        private uint HarnessShiftHC2_128_Int()
        {
            uint rl = 0;
            for (uint i = uint.MaxValue; i > uint.MaxValue - 512; i--)
                rl += i & 127;

            return rl;
        }

        private uint HarnessMersenneHC_3_Int()
        {
            uint rl = 0;
            uint mod3 = 0;
            for (uint i = uint.MaxValue; i > uint.MaxValue - 512; i--) {
                mod3 = (i >> 16) + (i & 0xFFFF);
                mod3 = (mod3 >> 8) + (mod3 & 0xFF);
                mod3 = (mod3 >> 4) + (mod3 & 0xF);
                mod3 = (mod3 >> 2) + (mod3 & 0x3);
                mod3 = (mod3 >> 2) + (mod3 & 0x3);
                mod3 = (mod3 >> 2) + (mod3 & 0x3);
                if (mod3 > 2) mod3 = mod3 - 3;
                rl += mod3;
            }

            return rl;
        }

        private uint HarnessMersenneHC_5_Int()
        {
            uint rl = 0;
            uint mod5 = 0;
            for (uint i = uint.MaxValue; i > uint.MaxValue - 512; i--) {
                mod5 = (i >> 16) + (i & 0xFFFF);
                mod5 = (mod5 >> 8) + (mod5 & 0xFF);
                mod5 = (mod5 >> 4) + (mod5 & 0xF);
                mod5 = (mod5 >> 4) + (mod5 & 0xF);
                if (mod5 > 14) { mod5 = mod5 - 15; } // mod 15
                if (mod5 > 10) { mod5 = mod5 - 10; }
                if (mod5 > 4) { mod5 = mod5 - 5; }
                rl += mod5;
            }

            return rl;
        }

        static private readonly uint[] _imersenne6Table = { 0, 3, 4, 1, 2, 5 };
        private uint HarnessMersenneHC_6_Int()
        {
            uint rl = 0;
            uint mod3 = 0;
            for (uint i = uint.MaxValue; i > uint.MaxValue - 512; i--) {
                mod3 = (i >> 16) + (i & 0xFFFF);
                mod3 = (mod3 >> 8) + (mod3 & 0xFF);
                mod3 = (mod3 >> 4) + (mod3 & 0xF);
                mod3 = (mod3 >> 2) + (mod3 & 0x3);
                mod3 = (mod3 >> 2) + (mod3 & 0x3);
                mod3 = (mod3 >> 2) + (mod3 & 0x3);
                if (mod3 > 2) mod3 = mod3 - 3;
                rl += _imersenne6Table[(mod3 << 1) | (i & 0b1)];
            }

            return rl;
        }

        static private readonly uint[] _imersenne70Table = { 0, 35, 50, 15, 30, 65, 10, 45, 60, 25, 40, 5, 20, 55, 0, 0, 56, 21, 36, 1, 16, 51, 66, 31, 46, 11, 26, 61, 6, 41, 0, 0, 42, 7, 22, 57, 2, 37, 52, 17, 32, 67, 12, 47, 62, 27, 0, 0, 28, 63, 8, 43, 58, 23, 38, 3, 18, 53, 68, 33, 48, 13, 0, 0, 14, 49, 64, 29, 44, 9, 24, 59, 4, 39, 54, 19, 34, 69 };
        private uint HarnessMersenneHC_70_Int()
        {
            uint rl = 0;
            uint mod5 = 0, mod7 = 0;
            for (uint i = uint.MaxValue; i > uint.MaxValue - 512; i--) {
                mod7 = (i >> 24) + (i & 0xFFFFFF);
                mod7 = (mod7 >> 12) + (mod7 & 0xFFF);
                mod7 = (mod7 >> 6) + (mod7 & 0x3F);
                mod7 = (mod7 >> 3) + (mod7 & 0x7);
                mod7 = (mod7 >> 3) + (mod7 & 0x7);
                if (mod7 > 6) mod7 = mod7 - 7;
                mod5 = (i >> 16) + (i & 0xFFFF);
                mod5 = (mod5 >> 8) + (mod5 & 0xFF);
                mod5 = (mod5 >> 4) + (mod5 & 0xF);
                mod5 = (mod5 >> 4) + (mod5 & 0xF);
                if (mod5 > 14) mod5 = mod5 - 15;
                if (mod5 > 9) mod5 = mod5 - 10;
                if (mod5 > 4) mod5 = mod5 - 5;
                rl += _imersenne70Table[(mod5 << 4) | (mod7 << 1) | (i & 0b1)];
            }

            return rl;
        }
    }
}
