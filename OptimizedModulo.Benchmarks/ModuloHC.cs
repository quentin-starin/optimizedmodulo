﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BenchmarkDotNet.Attributes;


namespace OptimizedModulo.Benchmarks
{
    public class ModuloHC
    {
        private readonly ulong _divisor16;
        private readonly ulong _dividend;
        
        public ModuloHC()
        {
            // don't use a constant values as the compiler might optimize the whole thing away
            var counter = new List<string>();
            counter.Add("");
            _divisor16 = (ulong)counter.Count() + 15;
            _dividend = (ulong)new Random().Next();
        }

        [Benchmark]
        public ulong Local_Modulo_HC() => _dividend % 5;

        [Benchmark]
        public ulong Local_Func_Modulo_HC() => ModuloOperator(_dividend, 5);

        [Benchmark]
        public ulong Local_Mersenne()
        {
            ulong dividend = (_dividend >> 32) + (_dividend & 0xFFFFFFFF);
            dividend = (dividend >> 16) + (dividend & 0xFFFF);
            dividend = (dividend >> 8) + (dividend & 0xFF);
            dividend = (dividend >> 4) + (dividend & 0xF);
            dividend = (dividend >> 4) + (dividend & 0xF);
            if (dividend > 14) dividend = dividend - 15;
            if (dividend > 9) dividend = dividend - 10;
            if (dividend > 4) dividend = dividend - 5;
            return dividend;
        }

        [Benchmark]
        public ulong Mersenne() => Modulo.Implementations.Long.Mersenne5(_dividend);

        // Modulo.Implementations.Long has the same implementations as found here (with MethodImplOptions.AggressiveInlining), just in another assembly

        [Benchmark]
        public ulong Pow2_Local_Modulo_HC() => _dividend % 16;

        [Benchmark]
        public ulong Pow2_Local_Func_Modulo_HC() => ModuloOperator(_dividend, 16);

        [Benchmark]
        public ulong Pow2_Local_Func_Modulo() => ModuloOperator(_dividend, _divisor16);

        [Benchmark]
        public ulong Pow2_Modulo_HC() => Modulo.Implementations.Long.ModuloOperator(_dividend, 16);

        [Benchmark]
        public ulong Pow2_Modulo() => Modulo.Implementations.Long.ModuloOperator(_dividend, _divisor16);

        [Benchmark]
        public ulong Pow2_Local_And_HC() => _dividend & 15;

        [Benchmark]
        public ulong Pow2_Local_Func_And_HC() => ShiftAnd(_dividend, 16);

        [Benchmark]
        public ulong Pow2_Local_Func_And() => ShiftAnd(_dividend, _divisor16);

        [Benchmark]
        public ulong Pow2_And_HC() => Modulo.Implementations.Long.ShiftAnd(_dividend, 16);

        [Benchmark]
        public ulong Pow2_And() => Modulo.Implementations.Long.ShiftAnd(_dividend, _divisor16);


        static public ulong ModuloOperator(ulong dividend, ulong divisor) => (dividend % divisor);

        static public ulong ShiftAnd(ulong dividend, ulong divisor) => (dividend & (divisor - 1));
    }
}
