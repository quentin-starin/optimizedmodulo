<Query Kind="Program" />

void Main()
{
	var m = 84;
	var f1 = 7;
	var f2 = 3;
	var f3 = 4;

	var r1 = new StringBuilder();
	var r2 = new StringBuilder();
	var r3 = new StringBuilder();
	var r4 = new StringBuilder();
	var r7 = new StringBuilder();
	var r = new List<Info>();
	for (int i = 0; i < m; i++)
	{
		r.Add(new Info
		{
			Num = i,
			Rem = i % m,
			F1 = i % f1,
			F2 = i % f2,
			F3 = i % f3,
			Index = (i % f1 << 4) | (i % f2 << 2) | i % f3
		});
	}

	foreach (var info in r.OrderBy(o => o.Index))
	{
		r2.Append(info.Index.ToString().PadLeft(4));
		r1.Append(info.Num.ToString().PadLeft(4));
		r3.Append(info.F1.ToString().PadLeft(4));
		r4.Append(info.F2.ToString().PadLeft(4));
		r7.Append(info.F3.ToString().PadLeft(4));
	}
	r1.AppendLine();
	r2.AppendLine();
	r3.AppendLine();
	r4.AppendLine();
	r7.AppendLine();

	r2.ToString().Dump();
	r1.ToString().Dump();
	r3.ToString().Dump();
	r4.ToString().Dump();
	r7.ToString().Dump();
	"".Dump();

	var r5 = new StringBuilder();
	var r6 = new StringBuilder();
	var r8 = new StringBuilder();
	for (int i = 0; i <= r.Max(o => o.Index); i++)
	{
		r5.Append($"{i}".PadLeft(6));
		var info = r.FirstOrDefault(o => o.Index == i);
		if (info == null)
		{
			r6.Append("0,".PadLeft(6));
			r8.Append("0, ");
		}
		else
		{
			r6.Append($"{info.Num},".PadLeft(6));
			r8.Append($"{info.Num}, ");
		}
	}
	"".Dump();
	r8.Length -= 2;
	r5.ToString().Dump();
	"".Dump();
	r6.ToString().Dump();
	"".Dump();
	"".Dump();
	r8.ToString().Dump();
}

class Info
{
	public int Num;
	public int Rem;
	public int F1;
	public int F2;
	public int F3;
	public int Index;
}