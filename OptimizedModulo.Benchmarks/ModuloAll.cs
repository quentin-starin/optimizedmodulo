﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BenchmarkDotNet.Attributes;


namespace OptimizedModulo.Benchmarks
{
    public class ModuloAll
    {
        private int _num25;
        private uint _uinum25;
        private ulong _ulnum25;
        private int _numLarge;
        private uint _uinumLarge;
        private ulong _ulnumLarge;
        private int _div3;
        private int _div4;
        private int _div5;
        private int _div6;
        private int _div7;
        private int _div8;
        private int _div10;
        private int _div12;
        private int _div14;
        private int _div15;
        private int _div21;
        private int _div35;
        private int _div42;
        private int _div70;

        public ModuloAll()
        {
            // make sure the compiler isn't optimizing based on constants differently than it could in real use
            // i.e. if we just did _div = 2; it would optimise away the modulo and replace it with a bitwise AND
            var counter = new List<object>();
            counter.AddRange(Enumerable.Repeat(new object(), 25));
            _num25 = counter.Count();
            _numLarge = _num25 + 35825232;

            counter.Clear();
            counter.AddRange(Enumerable.Repeat(new object(), 3));
            _div3 = counter.Count();
            counter.Add(new object());
            _div4 = counter.Count();
            counter.Add(new object());
            _div5 = counter.Count();
            _div6 = _div5 + 1;
            _div7 = _div5 + 2;
            _div10 = _div5 * 2;
            _div12 = _div5 + 7;
            _div14 = _div5 + 9;
            _div15 = _div5 + 10;
            _div21 = _div5 + 16;
            _div35 = _div5 + 30;
            _div42 = _div5 + 37;
            _div70 = _div5 * 12;

            _uinum25 = (uint)_num25;
            _ulnum25 = (ulong)_num25;
            _uinumLarge = (uint)_numLarge;
            _ulnumLarge = (ulong)_numLarge;

            // leaders
            _Div3_Modulo_Int = () => (int)((uint)_num25 % (uint)_div3);
            _Div4_Modulo_Int = () => (int)((uint)_num25 % (uint)_div4);
            _Div5_Modulo_Int = () => (int)((uint)_num25 % (uint)_div5);
            _Div6_Modulo_Int = () => (int)((uint)_num25 % (uint)_div6);
            _Div7_Modulo_Int = () => (int)((uint)_num25 % (uint)_div7);
            _Div10_Modulo_Int = () => Modulo.Implementations.Int.ModuloOperator(_uinum25, (uint)_div10);
            _Div12_Modulo_Int = () => Modulo.Implementations.Int.ModuloOperator(_uinum25, (uint)_div12);
            _Div15_Modulo_Int = () => (int)((uint)_num25 % (uint)_div15);

            _Div3_BigN_Modulo_Int = () => (int)((uint)_numLarge % (uint)_div3);
            _Div4_BigN_Modulo_Int = () => (int)((uint)_numLarge % (uint)_div4);
            _Div5_BigN_Modulo_Int = () => (int)((uint)_numLarge % (uint)_div5);
            _Div6_BigN_Modulo_Int = () => (int)((uint)_numLarge % (uint)_div6);
            _Div7_BigN_Modulo_Int = () => (int)((uint)_numLarge % (uint)_div7);
            _Div10_BigN_Modulo_Int = () => Modulo.Implementations.Int.ModuloOperator(_uinumLarge, (uint)_div10);
            _Div12_BigN_Modulo_Int = () => Modulo.Implementations.Int.ModuloOperator(_uinumLarge, (uint)_div12);
            _Div15_BigN_Modulo_Int = () => (int)((uint)_numLarge % (uint)_div15);

            _Div4_ShiftAnd_Int = () => ModShiftAnd(_num25, _div4);
            _Div4_BigN_ShiftAnd_Int = () => ModShiftAnd(_numLarge, _div4);

            _Div3_Modulo_Long = () => (long)((ulong)_num25 % (ulong)_div3);
            _Div4_Modulo_Long = () => (long)((ulong)_num25 % (ulong)_div4);
            _Div5_Modulo_Long = () => Modulo.Implementations.Long.ModuloOperator(_ulnum25, (ulong)_div5);
            _Div6_Modulo_Long = () => (long)((ulong)_num25 % (ulong)_div6);
            _Div7_Modulo_Long = () => (long)((ulong)_num25 % (ulong)_div7);
            _Div10_Modulo_Long = () => Modulo.Implementations.Long.ModuloOperator(_ulnum25, (ulong)_div10);
            _Div12_Modulo_Long = () => Modulo.Implementations.Long.ModuloOperator(_ulnum25, (ulong)_div12);
            _Div14_Modulo_Long = () => Modulo.Implementations.Long.ModuloOperator(_ulnum25, (ulong)_div14);
            _Div15_Modulo_Long = () => (long)((ulong)_num25 % (ulong)_div15);
            _Div21_Modulo_Long = () => Modulo.Implementations.Long.ModuloOperator(_ulnum25, (ulong)_div21);
            _Div35_Modulo_Long = () => Modulo.Implementations.Long.ModuloOperator(_ulnum25, (ulong)_div35);
            _Div42_Modulo_Long = () => Modulo.Implementations.Long.ModuloOperator(_ulnum25, (ulong)_div42);
            _Div70_Modulo_Long = () => Modulo.Implementations.Long.ModuloOperator(_ulnum25, (ulong)_div70);

            _Div3_BigN_Modulo_Long = () => (long)((ulong)_numLarge % (ulong)_div3);
            _Div4_BigN_Modulo_Long = () => (long)((ulong)_numLarge % (ulong)_div4);
            _Div5_BigN_Modulo_Long = () => Modulo.Implementations.Long.ModuloOperator(_ulnumLarge, (ulong)_div5);
            _Div6_BigN_Modulo_Long = () => (long)((ulong)_numLarge % (ulong)_div6);
            _Div7_BigN_Modulo_Long = () => (long)((ulong)_numLarge % (ulong)_div7);
            _Div10_BigN_Modulo_Long = () => Modulo.Implementations.Long.ModuloOperator(_ulnumLarge, (ulong)_div10);
            _Div12_BigN_Modulo_Long = () => Modulo.Implementations.Long.ModuloOperator(_ulnumLarge, (ulong)_div12);
            _Div15_BigN_Modulo_Long = () => (long)((ulong)_numLarge % (ulong)_div15);
            _Div35_BigN_Modulo_Long = () => Modulo.Implementations.Long.ModuloOperator(_ulnumLarge, (ulong)_div35);
            _Div42_BigN_Modulo_Long = () => Modulo.Implementations.Long.ModuloOperator(_ulnumLarge, (ulong)_div42);
            _Div70_BigN_Modulo_Long = () => Modulo.Implementations.Long.ModuloOperator(_ulnumLarge, (ulong)_div70);

            _Div4_ShiftAnd_Long = () => ModShiftAnd(_num25, _div4);
            _Div4_BigN_ShiftAnd_Long = () => ModShiftAnd(_numLarge, _div4);

            // test
            _Div3_Mersenne_Int = () => Mersenne3((uint)_num25);
            _Div5_Mersenne_1_Int = () => Mersenne5_1((uint)_num25);
            _Div5_Mersenne_2_Int = () => Mersenne5_2((uint)_num25);
            _Div5_Mersenne_3_Int = () => Mersenne5_3((uint)_num25);
            _Div6_Mersenne_1_Int = () => Mersenne6_1(_num25);
            _Div6_Mersenne_2_Int = () => Mersenne6_2(_num25);
            _Div6_Mersenne_2Mod_Int = () => Mersenne6_2Mod((uint)_num25);
            _Div7_Mersenne_Int = () => Mersenne7((uint)_num25);
            _Div10_Mersenne_Int = () => Modulo.Implementations.Int.Mersenne10(_uinum25);
            _Div12_Mersenne_Int = () => Modulo.Implementations.Int.Mersenne12(_uinum25);
            _Div15_Mersenne_Int = () => Mersenne15((uint)_num25);

            _Div3_BigN_Mersenne_Int = () => Mersenne3((uint)_numLarge);
            _Div5_BigN_Mersenne_1_Int = () => Mersenne5_1((uint)_numLarge);
            _Div5_BigN_Mersenne_2_Int = () => Mersenne5_2((uint)_numLarge);
            _Div5_BigN_Mersenne_3_Int = () => Mersenne5_3((uint)_numLarge);
            _Div6_BigN_Mersenne_1_Int = () => Mersenne6_1(_numLarge);
            _Div6_BigN_Mersenne_2_Int = () => Mersenne6_2(_numLarge);
            _Div6_BigN_Mersenne_2Mod_Int = () => Mersenne6_2Mod((uint)_numLarge);
            _Div7_BigN_Mersenne_Int = () => Mersenne7((uint)_numLarge);
            _Div10_BigN_Mersenne_Int = () => Modulo.Implementations.Int.Mersenne10(_uinumLarge);
            _Div12_BigN_Mersenne_Int = () => Modulo.Implementations.Int.Mersenne12(_uinumLarge);
            _Div15_BigN_Mersenne_Int = () => Mersenne15((uint)_numLarge);

            _Div3_Mersenne_Long = () => Mersenne3(_ulnum25);
            _Div5_Mersenne_1_Long = () => Modulo.Implementations.Long.Mersenne5(_ulnum25);
            //_Div5_Mersenne_2_Long = () => Modulo.Implementations.Long.Mersenne5_2(_ulnum25);
            //_Div5_Mersenne_2_2_Long = () => Mersenne5_2_2((ulong)_num25);
            //_Div5_Mersenne_3_Long = () => Modulo.Implementations.Long.Mersenne5_3(_ulnum25);
            _Div6_Mersenne_1_Long = () => Mersenne6_1((long)_num25);
            //_Div6_Mersenne_2_Long = () => Mersenne6_2((long)_num25);
            //_Div6_Mersenne_2Mod_Long = () => Mersenne6_2Mod((ulong)_num25);
            _Div7_Mersenne_Long = () => Mersenne7(_ulnum25);
            _Div10_Mersenne_Long = () => Modulo.Implementations.Long.Mersenne10(_ulnum25);
            //_Div10_Mersenne_2_Long = () => Modulo.Implementations.Long.Mersenne10_2(_ulnum25);
            //_Div10_Mersenne_3_Long = () => Modulo.Implementations.Long.Mersenne10_3(_ulnum25);
            _Div12_Mersenne_Long = () => Modulo.Implementations.Long.Mersenne12(_ulnum25);
            //_Div12_Mersenne_2_Long = () => Modulo.Implementations.Long.Mersenne12_2(_ulnum25);
            //_Div12_Mersenne_3_Long = () => Modulo.Implementations.Long.Mersenne12_3(_ulnum25);
            _Div14_Mersenne_Long = () => Modulo.Implementations.Long.Mersenne14(_ulnum25);
            //_Div14_Mersenne_2_Long = () => Modulo.Implementations.Long.Mersenne14_2(_ulnum25);
            //_Div14_Mersenne_3_Long = () => Modulo.Implementations.Long.Mersenne14_3(_ulnum25);
            _Div15_Mersenne_Long = () => Mersenne15(_ulnum25);
            _Div21_Mersenne_Long = () => Modulo.Implementations.Long.Mersenne21(_ulnum25);
            //_Div21_Mersenne_2_Long = () => Modulo.Implementations.Long.Mersenne21_2(_ulnum25);
            _Div35_Mersenne_Long = () => Modulo.Implementations.Long.Mersenne35(_ulnumLarge);
            _Div42_Mersenne_Long = () => Modulo.Implementations.Long.Mersenne42(_ulnumLarge);
            //_Div42_Mersenne_2_Long = () => Modulo.Implementations.Long.Mersenne42_2(_ulnumLarge);
            _Div70_Mersenne_Long = () => Modulo.Implementations.Long.Mersenne70(_ulnumLarge);

            _Div3_BigN_Mersenne_Long = () => Mersenne3(_ulnumLarge);
            _Div5_BigN_Mersenne_1_Long = () => Modulo.Implementations.Long.Mersenne5(_ulnumLarge);
            //_Div5_BigN_Mersenne_2_Long = () => Modulo.Implementations.Long.Mersenne5_2(_ulnumLarge);
            //_Div5_BigN_Mersenne_2_2_Long = () => Mersenne5_2_2((ulong)_numLarge);
            //_Div5_BigN_Mersenne_3_Long = () => Mersenne5_3((ulong)_numLarge);
            _Div6_BigN_Mersenne_1_Long = () => Mersenne6_1((long)_numLarge);
            //_Div6_BigN_Mersenne_2_Long = () => Mersenne6_2((long)_numLarge);
            //_Div6_BigN_Mersenne_2Mod_Long = () => Mersenne6_2Mod((ulong)_numLarge);
            _Div7_BigN_Mersenne_Long = () => Mersenne7(_ulnumLarge);
            _Div10_BigN_Mersenne_Long = () => Modulo.Implementations.Long.Mersenne10(_ulnumLarge);
            _Div12_BigN_Mersenne_Long = () => Modulo.Implementations.Long.Mersenne12(_ulnumLarge);
            _Div15_BigN_Mersenne_Long = () => Mersenne15(_ulnumLarge);
            _Div35_BigN_Mersenne_Long = () => Modulo.Implementations.Long.Mersenne35(_ulnumLarge);
            _Div42_BigN_Mersenne_Long = () => Modulo.Implementations.Long.Mersenne42(_ulnumLarge);
            //_Div42_BigN_Mersenne_2_Long = () => Modulo.Implementations.Long.Mersenne42_2(_ulnumLarge);
            _Div70_BigN_Mersenne_Long = () => Modulo.Implementations.Long.Mersenne70(_ulnumLarge);
        }

        [Benchmark] public uint Div12_Modulo_HC_Int() => _uinum25 % (uint)_div12;
        [Benchmark] public uint Div12_BigN_Modulo_HC_Int() => _uinumLarge % (uint)_div12;
        [Benchmark(Baseline = true)] public int Div3_Modulo_Int() => _Div3_Modulo_Int();
        [Benchmark] public int Div3_Mersenne_Int() => _Div3_Mersenne_Int();
        [Benchmark] public int Div3_BigN_Modulo_Int() => _Div3_BigN_Modulo_Int();
        [Benchmark] public int Div3_BigN_Mersenne_Int() => _Div3_BigN_Mersenne_Int();
        [Benchmark] public int Div4_Modulo_Int() => _Div4_Modulo_Int();
        [Benchmark] public int Div4_ShiftAnd_Int() => _Div4_ShiftAnd_Int();
        [Benchmark] public int Div4_BigN_Modulo_Int() => _Div4_BigN_Modulo_Int();
        [Benchmark] public int Div4_BigN_ShiftAnd_Int() => _Div4_BigN_ShiftAnd_Int();
        [Benchmark] public int Div5_Modulo_Int() => _Div5_Modulo_Int();
        [Benchmark] public int Div5_Mersenne_Int() => _Div5_Mersenne_1_Int();
        //[Benchmark] public int Div5_Mersenne_2_Int() => _Div5_Mersenne_2_Int();
        //[Benchmark] public int Div5_Mersenne_3_Int() => _Div5_Mersenne_3_Int();
        [Benchmark] public int Div5_BigN_Modulo_Int() => _Div5_BigN_Modulo_Int();
        [Benchmark] public int Div5_BigN_Mersenne_Int() => _Div5_BigN_Mersenne_1_Int();
        //[Benchmark] public int Div5_BigN_Mersenne_2_Int() => _Div5_BigN_Mersenne_2_Int();
        //[Benchmark] public int Div5_BigN_Mersenne_3_Int() => _Div5_BigN_Mersenne_3_Int();
        [Benchmark] public int Div6_Modulo_Int() => _Div6_Modulo_Int();
        [Benchmark] public int Div6_Mersenne_Int() => _Div6_Mersenne_1_Int();
        //[Benchmark] public int Div6_Mersenne_2_Int() => _Div6_Mersenne_2_Int();
        //[Benchmark] public int Div6_Mersenne_2Mod_Int() => _Div6_Mersenne_2Mod_Int();
        [Benchmark] public int Div6_BigN_Modulo_Int() => _Div6_BigN_Modulo_Int();
        [Benchmark] public int Div6_BigN_Mersenne_Int() => _Div6_BigN_Mersenne_1_Int();
        //[Benchmark] public int Div6_BigN_Mersenne_2_Int() => _Div6_BigN_Mersenne_2_Int();
        //[Benchmark] public int Div6_BigN_Mersenne_2Mod_Int() => _Div6_BigN_Mersenne_2Mod_Int();
        [Benchmark] public int Div7_Modulo_Int() => _Div7_Modulo_Int();
        [Benchmark] public int Div7_Mersenne_Int() => _Div7_Mersenne_Int();
        [Benchmark] public int Div7_BigN_Modulo_Int() => _Div7_BigN_Modulo_Int();
        [Benchmark] public int Div7_BigN_Mersenne_Int() => _Div7_BigN_Mersenne_Int();
        [Benchmark] public uint Div10_Modulo_Int() => _Div10_Modulo_Int();
        [Benchmark] public uint Div10_Mersenne_Int() => _Div10_Mersenne_Int();
        [Benchmark] public uint Div10_BigN_Modulo_Int() => _Div10_BigN_Modulo_Int();
        [Benchmark] public uint Div10_BigN_Mersenne_Int() => _Div10_BigN_Mersenne_Int();
        [Benchmark] public uint Div12_Modulo_Int() => _Div12_Modulo_Int();
        [Benchmark] public uint Div12_Mersenne_Int() => _Div12_Mersenne_Int();
        [Benchmark] public uint Div12_BigN_Modulo_Int() => _Div12_BigN_Modulo_Int();
        [Benchmark] public uint Div12_BigN_Mersenne_Int() => _Div12_BigN_Mersenne_Int();
        [Benchmark] public int Div15_Modulo_Int() => _Div15_Modulo_Int();
        [Benchmark] public int Div15_Mersenne_Int() => _Div15_Mersenne_Int();
        [Benchmark] public int Div15_BigN_Modulo_Int() => _Div15_BigN_Modulo_Int();
        [Benchmark] public int Div15_BigN_Mersenne_Int() => _Div15_BigN_Mersenne_Int();


        [Benchmark] public ulong Div12_Modulo_HC_Long() => _ulnum25 % (ulong)_div12;
        [Benchmark] public ulong Div12_BigN_Modulo_HC_Long() => _ulnumLarge % (ulong)_div12;
        [Benchmark] public long Div3_Modulo_Long() => _Div3_Modulo_Long();
        [Benchmark] public long Div3_Mersenne_Long() => _Div3_Mersenne_Long();
        [Benchmark] public long Div3_BigN_Modulo_Long() => _Div3_BigN_Modulo_Long();
        [Benchmark] public long Div3_BigN_Mersenne_Long() => _Div3_BigN_Mersenne_Long();
        [Benchmark] public long Div4_Modulo_Long() => _Div4_Modulo_Long();
        [Benchmark] public long Div4_ShiftAnd_Long() => _Div4_ShiftAnd_Long();
        [Benchmark] public long Div4_BigN_Modulo_Long() => _Div4_BigN_Modulo_Long();
        [Benchmark] public long Div4_BigN_ShiftAnd_Long() => _Div4_BigN_ShiftAnd_Long();
        [Benchmark] public ulong Div5_Modulo_Long() => _Div5_Modulo_Long();
        [Benchmark] public ulong Div5_Mersenne_Long() => _Div5_Mersenne_1_Long();
        //[Benchmark] public ulong Div5_Mersenne_2_Long() => _Div5_Mersenne_2_Long();
        //[Benchmark] public long Div5_Mersenne_2_2_Long() => _Div5_Mersenne_2_2_Long();
        //[Benchmark] public ulong Div5_Mersenne_3_Long() => _Div5_Mersenne_3_Long();
        [Benchmark] public ulong Div5_BigN_Modulo_Long() => _Div5_BigN_Modulo_Long();
        [Benchmark] public ulong Div5_BigN_Mersenne_Long() => _Div5_BigN_Mersenne_1_Long();
        //[Benchmark] public ulong Div5_BigN_Mersenne_2_Long() => _Div5_BigN_Mersenne_2_Long();
        //[Benchmark] public long Div5_BigN_Mersenne_2_2_Long() => _Div5_BigN_Mersenne_2_2_Long();
        //[Benchmark] public long Div5_BigN_Mersenne_3_Long() => _Div5_BigN_Mersenne_3_Long();
        [Benchmark] public long Div6_Modulo_Long() => _Div6_Modulo_Long();
        [Benchmark] public long Div6_Mersenne_Long() => _Div6_Mersenne_1_Long();
        //[Benchmark] public long Div6_Mersenne_2_Long() => _Div6_Mersenne_2_Long();
        //[Benchmark] public long Div6_Mersenne_2Mod_Long() => _Div6_Mersenne_2Mod_Long();
        [Benchmark] public long Div6_BigN_Modulo_Long() => _Div6_BigN_Modulo_Long();
        [Benchmark] public long Div6_BigN_Mersenne_Long() => _Div6_BigN_Mersenne_1_Long();
        //[Benchmark] public long Div6_BigN_Mersenne_2_Long() => _Div6_BigN_Mersenne_2_Long();
        //[Benchmark] public long Div6_BigN_Mersenne_2Mod_Long() => _Div6_BigN_Mersenne_2Mod_Long();
        [Benchmark] public long Div7_Modulo_Long() => _Div7_Modulo_Long();
        [Benchmark] public long Div7_Mersenne_Long() => _Div7_Mersenne_Long();
        [Benchmark] public long Div7_BigN_Modulo_Long() => _Div7_BigN_Modulo_Long();
        [Benchmark] public long Div7_BigN_Mersenne_Long() => _Div7_BigN_Mersenne_Long();
        [Benchmark] public ulong Div10_Modulo_Long() => _Div10_Modulo_Long();
        [Benchmark] public ulong Div10_Mersenne_Long() => _Div10_Mersenne_Long();
        //[Benchmark] public ulong Div10_Mersenne_2_Long() => _Div10_Mersenne_2_Long();
        //[Benchmark] public ulong Div10_Mersenne_3_Long() => _Div10_Mersenne_3_Long();
        [Benchmark] public ulong Div10_BigN_Modulo_Long() => _Div10_BigN_Modulo_Long();
        [Benchmark] public ulong Div10_BigN_Mersenne_Long() => _Div10_BigN_Mersenne_Long();
        [Benchmark] public ulong Div12_Modulo_Long() => _Div12_Modulo_Long();
        [Benchmark] public ulong Div12_Mersenne_Long() => _Div12_Mersenne_Long();
        //[Benchmark] public ulong Div12_Mersenne_2_Long() => _Div12_Mersenne_2_Long();
        //[Benchmark] public ulong Div12_Mersenne_3_Long() => _Div12_Mersenne_3_Long();
        [Benchmark] public ulong Div12_BigN_Modulo_Long() => _Div12_BigN_Modulo_Long();
        [Benchmark] public ulong Div12_BigN_Mersenne_Long() => _Div12_BigN_Mersenne_Long();
        [Benchmark] public ulong Div14_Modulo_Long() => _Div14_Modulo_Long();
        [Benchmark] public ulong Div14_Mersenne_Long() => _Div14_Mersenne_Long();
        //[Benchmark] public ulong Div14_Mersenne_2_Long() => _Div14_Mersenne_2_Long();
        //[Benchmark] public ulong Div14_Mersenne_3_Long() => _Div14_Mersenne_3_Long();
        [Benchmark] public long Div15_Modulo_Long() => _Div15_Modulo_Long();
        [Benchmark] public long Div15_Mersenne_Long() => _Div15_Mersenne_Long();
        [Benchmark] public long Div15_BigN_Modulo_Long() => _Div15_BigN_Modulo_Long();
        [Benchmark] public long Div15_BigN_Mersenne_Long() => _Div15_BigN_Mersenne_Long();
        [Benchmark] public ulong Div21_Modulo_Long() => _Div21_Modulo_Long();
        [Benchmark] public ulong Div21_Mersenne_Long() => _Div21_Mersenne_Long();
        //[Benchmark] public ulong Div21_Mersenne_2_Long() => _Div21_Mersenne_2_Long();
        [Benchmark] public ulong Div35_Modulo_Long() => _Div35_Modulo_Long();
        [Benchmark] public ulong Div35_Mersenne_Long() => _Div35_Mersenne_Long();
        [Benchmark] public ulong Div35_BigN_Modulo_Long() => _Div35_BigN_Modulo_Long();
        [Benchmark] public ulong Div35_BigN_Mersenne_Long() => _Div35_BigN_Mersenne_Long();
        [Benchmark] public ulong Div42_Modulo_HC_Long() => _ulnum25 % (ulong)_div42;
        [Benchmark] public ulong Div42_BigN_Modulo_HC_Long() => _ulnumLarge % (ulong)_div42;
        [Benchmark] public ulong Div42_Modulo_Long() => _Div42_Modulo_Long();
        [Benchmark] public ulong Div42_Mersenne_Long() => _Div42_Mersenne_Long();
        //[Benchmark] public ulong Div42_Mersenne_2_Long() => _Div42_Mersenne_2_Long();
        [Benchmark] public ulong Div42_BigN_Modulo_Long() => _Div42_BigN_Modulo_Long();
        [Benchmark] public ulong Div42_BigN_Mersenne_Long() => _Div42_BigN_Mersenne_Long();
        //[Benchmark] public ulong Div42_BigN_Mersenne_2_Long() => _Div42_BigN_Mersenne_2_Long();
        [Benchmark] public ulong Div70_Modulo_Long() => _Div70_Modulo_Long();
        [Benchmark] public ulong Div70_Mersenne_Long() => _Div70_Mersenne_Long();
        [Benchmark] public ulong Div70_BigN_Modulo_Long() => _Div70_BigN_Modulo_Long();
        [Benchmark] public ulong Div70_BigN_Mersenne_Long() => _Div70_BigN_Mersenne_Long();


        private readonly Func<int> _Div3_Modulo_Int;
        private readonly Func<int> _Div3_Mersenne_Int;
        private readonly Func<int> _Div3_BigN_Modulo_Int;
        private readonly Func<int> _Div3_BigN_Mersenne_Int;
        private readonly Func<int> _Div4_Modulo_Int;
        private readonly Func<int> _Div4_ShiftAnd_Int;
        private readonly Func<int> _Div4_BigN_Modulo_Int;
        private readonly Func<int> _Div4_BigN_ShiftAnd_Int;
        private readonly Func<int> _Div5_Modulo_Int;
        private readonly Func<int> _Div5_Mersenne_1_Int;
        private readonly Func<int> _Div5_Mersenne_2_Int;
        private readonly Func<int> _Div5_Mersenne_3_Int;
        private readonly Func<int> _Div5_BigN_Modulo_Int;
        private readonly Func<int> _Div5_BigN_Mersenne_1_Int;
        private readonly Func<int> _Div5_BigN_Mersenne_2_Int;
        private readonly Func<int> _Div5_BigN_Mersenne_3_Int;
        private readonly Func<int> _Div6_Modulo_Int;
        private readonly Func<int> _Div6_Mersenne_1_Int;
        private readonly Func<int> _Div6_Mersenne_2_Int;
        private readonly Func<int> _Div6_Mersenne_2Mod_Int;
        private readonly Func<int> _Div6_BigN_Modulo_Int;
        private readonly Func<int> _Div6_BigN_Mersenne_1_Int;
        private readonly Func<int> _Div6_BigN_Mersenne_2_Int;
        private readonly Func<int> _Div6_BigN_Mersenne_2Mod_Int;
        private readonly Func<int> _Div7_Modulo_Int;
        private readonly Func<int> _Div7_Mersenne_Int;
        private readonly Func<uint> _Div10_Modulo_Int;
        private readonly Func<uint> _Div10_Mersenne_Int;
        private readonly Func<uint> _Div10_BigN_Modulo_Int;
        private readonly Func<uint> _Div10_BigN_Mersenne_Int;
        private readonly Func<uint> _Div12_Modulo_Int;
        private readonly Func<uint> _Div12_Mersenne_Int;
        private readonly Func<uint> _Div12_BigN_Modulo_Int;
        private readonly Func<uint> _Div12_BigN_Mersenne_Int;
        private readonly Func<int> _Div7_BigN_Modulo_Int;
        private readonly Func<int> _Div7_BigN_Mersenne_Int;
        private readonly Func<int> _Div15_Modulo_Int;
        private readonly Func<int> _Div15_Mersenne_Int;
        private readonly Func<int> _Div15_BigN_Modulo_Int;
        private readonly Func<int> _Div15_BigN_Mersenne_Int;


        private readonly Func<long> _Div3_Modulo_Long;
        private readonly Func<long> _Div3_Mersenne_Long;
        private readonly Func<long> _Div3_BigN_Modulo_Long;
        private readonly Func<long> _Div3_BigN_Mersenne_Long;
        private readonly Func<long> _Div4_Modulo_Long;
        private readonly Func<long> _Div4_ShiftAnd_Long;
        private readonly Func<long> _Div4_BigN_Modulo_Long;
        private readonly Func<long> _Div4_BigN_ShiftAnd_Long;
        private readonly Func<ulong> _Div5_Modulo_Long;
        private readonly Func<ulong> _Div5_Mersenne_1_Long;
        private readonly Func<ulong> _Div5_Mersenne_2_Long;
        private readonly Func<long> _Div5_Mersenne_2_2_Long;
        private readonly Func<ulong> _Div5_Mersenne_3_Long;
        private readonly Func<ulong> _Div5_BigN_Modulo_Long;
        private readonly Func<ulong> _Div5_BigN_Mersenne_1_Long;
        private readonly Func<ulong> _Div5_BigN_Mersenne_2_Long;
        private readonly Func<long> _Div5_BigN_Mersenne_2_2_Long;
        private readonly Func<long> _Div5_BigN_Mersenne_3_Long;
        private readonly Func<long> _Div6_Modulo_Long;
        private readonly Func<long> _Div6_Mersenne_1_Long;
        private readonly Func<long> _Div6_Mersenne_2_Long;
        private readonly Func<long> _Div6_Mersenne_2Mod_Long;
        private readonly Func<long> _Div6_BigN_Modulo_Long;
        private readonly Func<long> _Div6_BigN_Mersenne_1_Long;
        private readonly Func<long> _Div6_BigN_Mersenne_2_Long;
        private readonly Func<long> _Div6_BigN_Mersenne_2Mod_Long;
        private readonly Func<long> _Div7_Modulo_Long;
        private readonly Func<long> _Div7_Mersenne_Long;
        private readonly Func<long> _Div7_BigN_Modulo_Long;
        private readonly Func<long> _Div7_BigN_Mersenne_Long;
        private readonly Func<ulong> _Div10_Modulo_Long;
        private readonly Func<ulong> _Div10_Mersenne_Long;
        private readonly Func<ulong> _Div10_Mersenne_2_Long;
        private readonly Func<ulong> _Div10_Mersenne_3_Long;
        private readonly Func<ulong> _Div10_BigN_Modulo_Long;
        private readonly Func<ulong> _Div10_BigN_Mersenne_Long;
        private readonly Func<ulong> _Div12_Modulo_Long;
        private readonly Func<ulong> _Div12_Mersenne_Long;
        private readonly Func<ulong> _Div12_Mersenne_2_Long;
        private readonly Func<ulong> _Div12_Mersenne_3_Long;
        private readonly Func<ulong> _Div12_BigN_Modulo_Long;
        private readonly Func<ulong> _Div12_BigN_Mersenne_Long;
        private readonly Func<ulong> _Div14_Modulo_Long;
        private readonly Func<ulong> _Div14_Mersenne_Long;
        private readonly Func<ulong> _Div14_Mersenne_2_Long;
        private readonly Func<ulong> _Div14_Mersenne_3_Long;
        private readonly Func<long> _Div15_Modulo_Long;
        private readonly Func<long> _Div15_Mersenne_Long;
        private readonly Func<long> _Div15_BigN_Modulo_Long;
        private readonly Func<long> _Div15_BigN_Mersenne_Long;
        private readonly Func<ulong> _Div21_Modulo_Long;
        private readonly Func<ulong> _Div21_Mersenne_Long;
        private readonly Func<ulong> _Div21_Mersenne_2_Long;
        private readonly Func<ulong> _Div35_Modulo_Long;
        private readonly Func<ulong> _Div35_Mersenne_Long;
        private readonly Func<ulong> _Div35_BigN_Modulo_Long;
        private readonly Func<ulong> _Div35_BigN_Mersenne_Long;
        private readonly Func<ulong> _Div42_Modulo_Long;
        private readonly Func<ulong> _Div42_Mersenne_Long;
        private readonly Func<ulong> _Div42_Mersenne_2_Long;
        private readonly Func<ulong> _Div42_BigN_Modulo_Long;
        private readonly Func<ulong> _Div42_BigN_Mersenne_Long;
        private readonly Func<ulong> _Div42_BigN_Mersenne_2_Long;
        private readonly Func<ulong> _Div70_Modulo_Long;
        private readonly Func<ulong> _Div70_Mersenne_Long;
        private readonly Func<ulong> _Div70_BigN_Modulo_Long;
        private readonly Func<ulong> _Div70_BigN_Mersenne_Long;



        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private int ModShiftAnd(int num, int div) => (num & (div - 1));

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private int Mersenne3(uint a)
        {
            a = (a >> 16) + (a & 0xFFFF); // sum base 2**16 digits
            a = (a >> 8) + (a & 0xFF);    // sum base 2**8 digits
            a = (a >> 4) + (a & 0xF);     // sum base 2**4 digits
            a = (a >> 2) + (a & 0x3);     // sum base 2**2 digits
            a = (a >> 2) + (a & 0x3);     // sum base 2**2 digits
            a = (a >> 2) + (a & 0x3);     // sum base 2**2 digits
            if (a > 2) a = a - 3;
            return (int)a;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private int Mersenne5_1(uint a)
        {
            a = (a >> 16) + (a & 0xFFFF); /* sum base 2**16 digits */
            a = (a >> 8) + (a & 0xFF);   /* sum base 2**8 digits */
            a = (a >> 4) + (a & 0xF);    /* sum base 2**4 digits */
            if (a > 14) a = a - 15;
            if (a > 14) a = a - 15;
            if (a > 14) a = a - 15;       // now, we have mod 15 */
            /* now, we have mod 15 */
            if (a > 4) a = a - 5;
            if (a > 4) a = a - 5;
            return (int)a;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private int Mersenne5_2(uint a)
        {
            a = (a >> 16) + (a & 0xFFFF); /* sum base 2**16 digits */
            a = (a >> 8) + (a & 0xFF);   /* sum base 2**8 digits */
            a = (a >> 4) + (a & 0xF);    /* sum base 2**4 digits */
            if (a > 14) a = a - 15;
            if (a > 14) a = a - 15;
            if (a > 14) a = a - 15;       // now, we have mod 15 */
            /* now, we have mod 15 */
            if (a > 9) a = a - 10;
            else if (a > 4) a = a - 5;
            return (int)a;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private int Mersenne5_3(uint a)
        {
            a = (a >> 16) + (a & 0xFFFF); /* sum base 2**16 digits */
            a = (a >> 8) + (a & 0xFF);   /* sum base 2**8 digits */
            a = (a >> 4) + (a & 0xF);    /* sum base 2**4 digits */
            if (a > 14) a = a - 15;
            if (a > 14) a = a - 15;
            if (a > 14) a = a - 15;       // now, we have mod 15 */
            /* now, we have mod 15 */
            if (a > 9) return (int)(a - 10);
            if (a > 4) return (int)(a - 5);
            return (int)a;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private int Mersenne6_1(int a)
        {
            while (a > 11) {
                int s = 0; /* accumulator for the sum of the digits */
                while (a != 0) {
                    s = s + (a & 7);
                    a = ((a >> 2) & -2);
                }
                a = s;
            }
            /* note, at this point: a < 12 */
            if (a > 5) a = a - 6;
            return (int)a;
        }

        static private readonly int[] _mersenne6Table = new [] { 0, 3, 4, 1, 2, 5 };

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private int Mersenne6_2(int a)
            => _mersenne6Table[(Mersenne3((uint)a) << 1) | (a & 1)];

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private int Mersenne6_2Mod(uint a)
            => _mersenne6Table[((a % 3) << 1) | (a & 1)];

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private int Mersenne7(uint a)
        {
            a = (a >> 24) + (a & 0xFFFFFF); // sum base 2**24 digits
            a = (a >> 12) + (a & 0xFFF);    // sum base 2**12 digits
            a = (a >> 6) + (a & 0x3F);      // sum base 2**6 digits
            a = (a >> 3) + (a & 0x7);       // sum base 2**2 digits
            a = (a >> 2) + (a & 0x7);       // sum base 2**2 digits
            if (a > 5) a = a - 6;
            return (int)a;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private int Mersenne15(uint a)
        {
            a = (a >> 16) + (a & 0xFFFF); /* sum base 2**16 digits */
            a = (a >> 8) + (a & 0xFF);   /* sum base 2**8 digits */
            a = (a >> 4) + (a & 0xF);    /* sum base 2**4 digits */
            if (a < 15) return (int)a;
            if (a < (2 * 15)) return (int)(a - 15);
            if (a < (3 * 15)) return (int)(a - (2 * 15));
            return (int)(a - (3 * 15));
        }



        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private long Mersenne3(ulong a)
        {
            a = (a >> 32) + (a & 0xFFFFFFFF); // sum base 2**32 digits */
            a = (a >> 16) + (a & 0xFFFF); // sum base 2**16 digits
            a = (a >> 8) + (a & 0xFF);    // sum base 2**8 digits
            a = (a >> 4) + (a & 0xF);     // sum base 2**4 digits
            a = (a >> 2) + (a & 0x3);     // sum base 2**2 digits
            a = (a >> 2) + (a & 0x3);     // sum base 2**2 digits
            a = (a >> 2) + (a & 0x3);     // sum base 2**2 digits
            if (a > 2) a = a - 3;
            return (long)a;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private long Mersenne5_1(ulong a)
        {
            a = (a >> 32) + (a & 0xFFFFFFFF); // sum base 2**32 digits */
            a = (a >> 16) + (a & 0xFFFF); /* sum base 2**16 digits */
            a = (a >> 8) + (a & 0xFF);   /* sum base 2**8 digits */
            a = (a >> 4) + (a & 0xF);    /* sum base 2**4 digits */
            if (a > 14) a = a - 15;
            if (a > 14) a = a - 15;
            if (a > 14) a = a - 15;
            if (a > 14) a = a - 15;       // now, we have mod 15 */
            /* now, we have mod 15 */
            if (a > 4) a = a - 5;
            if (a > 4) a = a - 5;
            return (long)a;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private long Mersenne5_2(ulong a)
        {
            a = (a >> 32) + (a & 0xFFFFFFFF); // sum base 2**32 digits */
            a = (a >> 16) + (a & 0xFFFF); /* sum base 2**16 digits */
            a = (a >> 8) + (a & 0xFF);   /* sum base 2**8 digits */
            a = (a >> 4) + (a & 0xF);    /* sum base 2**4 digits */
            if (a > 14) a = a - 15;
            if (a > 14) a = a - 15;
            if (a > 14) a = a - 15;
            if (a > 14) a = a - 15;       // now, we have mod 15 */
            /* now, we have mod 15 */
            if (a > 9) a = a - 10;
            else if (a > 4) a = a - 5;
            return (long)a;
        }

        private long Mersenne5_2_2(ulong a)
        {
            a = (a >> 32) + (a & 0xFFFFFFFF); // sum base 2**32 digits */
            a = (a >> 16) + (a & 0xFFFF); /* sum base 2**16 digits */
            a = (a >> 8) + (a & 0xFF);   /* sum base 2**8 digits */
            a = (a >> 4) + (a & 0xF);    /* sum base 2**4 digits */
            if (a > 59) a = a - 60;
            else if (a > 44) a = a - 45;
            else if (a > 29) a = a - 30;
            else if (a > 14) a = a - 15;
            /* now, we have mod 15 */
            if (a > 9) a = a - 10;
            else if (a > 4) a = a - 5;
            return (long)a;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private long Mersenne5_3(ulong a)
        {
            a = (a >> 32) + (a & 0xFFFFFFFF); // sum base 2**32 digits */
            a = (a >> 16) + (a & 0xFFFF); /* sum base 2**16 digits */
            a = (a >> 8) + (a & 0xFF);   /* sum base 2**8 digits */
            a = (a >> 4) + (a & 0xF);    /* sum base 2**4 digits */
            if (a > 14) a = a - 15;
            if (a > 14) a = a - 15;
            if (a > 14) a = a - 15;
            if (a > 14) a = a - 15;       // now, we have mod 15 */
            /* now, we have mod 15 */
            if (a > 9) return (long)(a - 10);
            if (a > 4) return (long)(a - 5);
            return (long)a;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private long Mersenne6_1(long a)
        {
            while (a > 11) {
                long s = 0; /* accumulator for the sum of the digits */
                while (a != 0) {
                    s = s + (a & 7);
                    a = ((a >> 2) & -2);
                }
                a = s;
            }
            /* note, at this polong: a < 12 */
            if (a > 5) a = a - 6;
            return (long)a;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private long Mersenne6_2(long a)
            => _mersenne6Table[(Mersenne3((ulong)a) << 1) | (a & 1)];

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private long Mersenne6_2Mod(ulong a)
            => _mersenne6Table[((a % 3) << 1) | (a & 1)];

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private long Mersenne7(ulong a)
        {
            a = (a >> 48) + (a & 0xFFFFFFFFFFFF); // sum base 2**48 digits
            a = (a >> 24) + (a & 0xFFFFFF); // sum base 2**24 digits
            a = (a >> 12) + (a & 0xFFF);    // sum base 2**12 digits
            a = (a >> 6) + (a & 0x3F);      // sum base 2**6 digits
            a = (a >> 3) + (a & 0x7);       // sum base 2**2 digits
            a = (a >> 2) + (a & 0x7);       // sum base 2**2 digits
            if (a > 5) a = a - 6;
            return (long)a;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private long Mersenne15(ulong a)
        {
            a = (a >> 32) + (a & 0xFFFFFFFF); // sum base 2**32 digits */
            a = (a >> 16) + (a & 0xFFFF); /* sum base 2**16 digits */
            a = (a >> 8) + (a & 0xFF);   /* sum base 2**8 digits */
            a = (a >> 4) + (a & 0xF);    /* sum base 2**4 digits */
            if (a < 15) return (long)a;
            if (a < (2 * 15)) return (long)(a - 15);
            if (a < (3 * 15)) return (long)(a - (2 * 15));
            if (a < (4 * 15)) return (long)(a - (3 * 15));
            return (long)(a - (4 * 15));
        }
    }
}
