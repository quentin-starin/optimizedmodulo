﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;



namespace OptimizedModulo.Tests
{
    public partial class OptimizedModulo
    {
        [Theory]
        [InlineData((uint)0)]
        [InlineData((uint)1)]
        [InlineData((uint)2)]
        [InlineData((uint)3)]
        [InlineData((uint)4)]
        [InlineData((uint)5)]
        [InlineData((uint)6)]
        [InlineData((uint)7)]
        [InlineData((uint)8)]
        [InlineData((uint)65529)]
        [InlineData((uint)65530)]
        [InlineData((uint)65531)]
        [InlineData((uint)65532)]
        [InlineData((uint)65533)]
        [InlineData((uint)65534)]
        [InlineData((uint)65535)]
        [InlineData((uint)65536)]
        [InlineData((uint)65537)]
        [InlineData((uint)65538)]
        [InlineData((uint)65539)]
        [InlineData((uint)65540)]
        [InlineData((uint)65541)]
        [InlineData((uint)2147483644)]
        [InlineData((uint)2147483645)]
        [InlineData((uint)2147483646)]
        [InlineData((uint)2147483647)]
        [InlineData((uint)2147483648)]
        [InlineData((uint)2147483649)]
        [InlineData((uint)2147483650)]
        [InlineData((uint)4294967289)]
        [InlineData((uint)4294967290)]
        [InlineData((uint)4294967291)]
        [InlineData((uint)4294967292)]
        [InlineData((uint)4294967293)]
        [InlineData((uint)4294967294)]
        [InlineData((uint)4294967295)]
        [InlineData((ulong)4294967296)]
        [InlineData((ulong)4294967297)]
        [InlineData((ulong)4294967298)]
        [InlineData((ulong)4294967299)]
        [InlineData((ulong)4294967300)]
        [InlineData((ulong)4294967301)]
        [InlineData((ulong)9223372036854775801)]
        [InlineData((ulong)9223372036854775802)]
        [InlineData((ulong)9223372036854775803)]
        [InlineData((ulong)9223372036854775804)]
        [InlineData((ulong)9223372036854775805)]
        [InlineData((ulong)9223372036854775806)]
        [InlineData((ulong)9223372036854775807)]
        [InlineData((ulong)9223372036854775808)]
        [InlineData((ulong)9223372036854775809)]
        [InlineData((ulong)9223372036854775810)]
        [InlineData((ulong)9223372036854775811)]
        [InlineData((ulong)9223372036854775812)]
        [InlineData((ulong)9223372036854775813)]
        [InlineData((ulong)18446744073709551607)]
        [InlineData((ulong)18446744073709551608)]
        [InlineData((ulong)18446744073709551609)]
        [InlineData((ulong)18446744073709551610)]
        [InlineData((ulong)18446744073709551611)]
        [InlineData((ulong)18446744073709551612)]
        [InlineData((ulong)18446744073709551613)]
        [InlineData((ulong)18446744073709551614)]
        [InlineData((ulong)18446744073709551615)]
        public void ULong_Mersenne_0003(ulong d)
        {
            Assert.Equal(d % 3L, Modulo.Implementations.Long.Mersenne3(d));
        }
    }
}
