@echo off
setlocal EnableDelayedExpansion

del /f /q *.nupkg
call nuget-pack.cmd

nuget push -Source https://api.nuget.org/v3/index.json OptimizedModulo.?.?.?.nupkg %1
nuget push -Source https://nuget.smbsrc.net/ OptimizedModulo.?.?.?.symbols.nupkg %1
