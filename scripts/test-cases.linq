<Query Kind="Program" />

void Main()
{
	ulong m = 3;
	var isLong = m.GetType() == typeof(ulong);
	
	var sb = new StringBuilder();

	for (ulong i = 0; i <= m + 5; i++)
		Append(sb, i);

	if (m < 255)
		for (ulong i = ushort.MaxValue - (m/2 + 5); i <= ushort.MaxValue + (m/2 + 5); i++)
			Append(sb, i);

	if (m < 255)
		for (ulong i = (uint)int.MaxValue - m; i <= (uint)int.MaxValue + m; i++)
			Append(sb, i);
	
	if (isLong)
		for (ulong i = uint.MaxValue - (m/2 + 5); i <= uint.MaxValue + (m/2 + 5); i++)
			Append(sb, i);
	else
		for (ulong i = uint.MaxValue - m - 5; i <= uint.MaxValue; i++)
			Append(sb, i);

	if (isLong && m < 255)
		for (ulong i = (ulong)long.MaxValue - (m/2 + 5); i <= (ulong)long.MaxValue + (m/2 + 5); i++)
			Append(sb, i);

	if (isLong)
		for (ulong i = ulong.MaxValue - m - 5; i > 0; i++)
			Append(sb, i);

	sb.ToString().Dump();
	//ulong.MaxValue.Dump();
}

private void Append(StringBuilder sb, ulong i) => sb.AppendLine($"\t\t[InlineData(({(i > uint.MaxValue ? "ulong" : "uint")}){i})]");

// Define other methods and classes here
